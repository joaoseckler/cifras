Vou Partir - Nelson Cavaquinho | Jair do Cavaquinho
Intérprete: Nelson Cavaquinho
Álbum: Nelson Cavaquinho
Ano: 1973
Tom: D
Forma: AA B AA B, A à vontade
Obs: Nelson faz harmonia do B cada hora de um jeito
Tessitura: B2 - E4
BPM: 88
Cifraclub: https://www.cifraclub.com.br/nelson-cavaquinho/vou-partir/
Catálogo: https://immub.org/album/nelson-cavaquinho


D   F°   Em    A7
D   F°   Em    A7

[A]

D6    Fº   Em7
Vou   par__tir
           A7   A7/G   D
Não sei se vol__ta_____rei
          B7(9)   Em7
Tu não me queiras mal
       Gm6   A7   D     A7
Hoje é car___na___val

[B]

D                 A7
Partirei para bem longe
                     Gm6  A7
Não precisas te preocupar
Am7     D7      G
Só voltarei pra casa
E7                      A7
   Quando o carnaval acabar, acabar
