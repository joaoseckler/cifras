#!/usr/bin/env python3

from glob import glob
from pathlib import Path


def parse_header(content):
    lines = content.split("\n")
    header = {}

    header["title"] = lines[0].split(" - ")[0].strip()
    header["authors"] = list(
        map(str.strip, lines[0].split(" - ")[1].strip().split(" | "))
    )

    del lines[0]

    while ":" in lines[0]:
        data = lines[0].split(":", maxsplit=1)
        header[data[0].strip()] = data[1].strip()
        del lines[0]

    return header


def warn(file, msg):
    print(f"{file.name}: {msg}")
    print("---------------")


def check_interp_album(file, data):
    """Check if album author is redundantly added when it was already
    defined as the interpreter of the version
    """

    interpreters = data.get("Intérprete") or data.get("Intérpretes")
    album = data.get("Álbum")

    if interpreters is None or album is None:
        return

    album_data = album.split(" - ")
    if len(album_data) < 2:
        return

    album_authors = album_data[1].split(" | ")
    interpreters = interpreters.split(" | ")

    if set(album_authors) == set(interpreters):
        warn(
            file,
            "Album author is redundantly added, since it is already "
            "defined as the interpreter of version",
        )


def lint(file, content):
    data = parse_header(content)
    check_interp_album(file, data)


def main():
    for file in glob("txt/*"):
        with open(file, "r", encoding="utf-8") as f:
            lint(Path(file), f.read())


if __name__ == "__main__":
    main()
