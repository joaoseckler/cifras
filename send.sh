host=jseckler.xyz
folder=jseckler.xyz

pull=""
push=""
if [ "$1" = "--pull" ]; then
  pull=yes
  shift 1
elif [ "$1" = "--push" ]; then
  push=yes
  shift 1
elif [ "$1" = "--pp" ]; then
  pull=yes
  push=yes
  shift 1
fi

files=( "$@" )

for ((i=0; i < "${#files[@]}"; i++)); do
  files[i]="laialaia/raw/${files[i]}"
done

if [ -n "$push" ]; then
  git push
fi
cmd=""
cmd+="cd $folder; "
if [ -n "$pull" ]; then
  cmd+="cd laialaia/raw; git pull --rebase && cd - && "
fi
cmd+="set -a && . .env && set +a && . venv/bin/activate && "
cmd+="./manage.py register_cifra -u ${files[@]} && "
cmd+="systemctl restart gunicorn"

ssh "$host" "$cmd"
