Ainda é tempo pra ser feliz - Arlindo Cruz
Intérprete: Zeca Pagodinho | Beth Carvalho
Álbum: Zeca Pagodinho - Zeca Pagodinho
Ano: 1998
Tom: G
Forma: intro, ABB ABB, B à vontade
Tessitura: E3 - G4
BPM: 72
Cifraclub: https://www.cifraclub.com.br/zeca-pagodinho/ainda-tempo-de-ser-feliz/
Catálogo: https://immub.org/album/zeca-pagodinho-1

.  Am7    D7    Bm7(b5)    E7 Am7    D7    G7M   G7(9)   C7M
% \time 2/4 \clef treble \key g \major
% s4 \tuplet 3/2 { e,8 g b } | b a a g | a4 \tuplet 3/2 { a8 c e } |
% e8 d d cis  | d4 \tuplet 3/2 {e8 g b }
% b8 a a g | g8 fis fis e | fis4 \tuplet 3/2 { a8 g fis } |
% f4~ \tuplet 3/2 { f8 e f } | e2

[A]

C7M                    Cm6         Bm7  Dm6/F       E7(4)   E7
    Me cansei de ficar mudo sem tentar,       sem falar
C7M                      Cm6         B7(13)   B7(13b)
    Mas não posso deixar tudo como está
E7(9-)          G7
    Como está você?
           C7M                   D7/C
Tô vivendo por viver, tô cansada de chorar
               Bm7               E7(4)            E7
Não sei mais o que fazer, você tem que me ajudar
           Am7         Bm7            C7M        B7(#9)    Em(9)
Tá difícil esquecer,       impossível não lembrar        você
    Dm7           G7(13)
E você como está?
             C#m7(5-)                 D7/C
Com o fim do nosso amor, eu também tô por aí
               Bm7                     E7(4)        E7
Eu não sei pra onde vou, tantas noites sem dormir
       Am7        Bm7            C7M        D7(9-)    G7M   Dm7   G7
Alivia minha dor,      e me faça por favor        sorrir

[B]

              C7M        D/C          G7M    G7
Vem pros meus braços meu amor, meu acalanto
            C7M          D/C          Bm7(5-)  G7(9)
Leva esse pranto pra bem longe de nós dois
           C7M        D/C        Bm7(5-)       E7
Não deixe nada pra depois, é a saudade que me diz
            Am7       D7(9b)     ( G7M   C#7(#11) )
Que ainda é tempo pra viver feliz

( G7M   G7(9)   C7M )
% e4 \tuplet 3/2 { a8 g fis } | f4 \tuplet 3/2 { b8 a aes } | g2
