Toda a hora - Toninho Geraes | Moacyr Luz
Intérprete: Zeca Pagodinho
Álbum: Zeca Apresenta: o Quintal do Pagodinho
Ano: 2016
Forma: intro, AR BR ARR, intro
Tom: Dm
Tessitura: A2 - F4
BPM: 120
Cifraclub: https://www.cifraclub.com.br/zeca-pagodinho/toda-hora/
Catálogo: https://www.discogs.com/pt_BR/release/7047593-Various-Zeca-Apresenta-O-Quintal-Do-Pagodinho-Ao-Vivo


   Dm     A7               Dm   A7
% \nobreak
%  d4 f | e r16 g8 f16~ | f4 a | g4 r16 e8 f16~ |
  Dm    A7                Dm
% f4 d | e r16 cis8 d16~ | d2

[A]

      A7
(Toda hora)
     Dm             A7          Dm    D7
Toda hora alguém me chama pra beber
     Gm             D7          Gm
Toda hora alguém me chama pra zoar
   C7                          F   A7 Dm
Porque ninguém me chama pra benzer
   Bb             Bb7         A7
Porque ninguém me chama pra rezar

   Dm          A7            Dm     D7
Só vou pra batizado quando é samba
   Gm           D7       Gm
Compadre meu precisa batucar
   C7                     F  A7 Dm
Eu sou da saideira que descamba
Bb           Bb7         A7
Aqui não tem hora pra acabar

[R]

 Dm            A7          Dm
Amigo eu nunca fiz bebendo leite
 C              C7         F   D7
Amigo eu não criei bebendo chá
   Gm          G#º         Dm    Bb
Eu sou da madrugada, me respeite
   E7         A7         Dm   A7
Eu sei a hora de ir trabalhar

[B]

           Dm        A7             Dm    D7
Não sou sujeito de ficar enchendo a cara
          Gm       D7           Gm
Quem escancara não vê o mundo girar
          C7                  F   A7  Dm
Pra ficar bom, é melhor tomar remédio
      Bb             Bb7          A7
O meu tédio é quando vem fechar o bar

   Dm            A7          Dm     D7
Também não vou ficar levando bronca
        Gm                D7        Gm
Feche a conta, hoje eu preciso pendurar
        C7                    F A7 Dm
Já calibrei, já tirei a minha onda
Bb          Bb7         A7
Ainda tenho casa pra cuidar
