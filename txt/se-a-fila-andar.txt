Se a fila andar - Paulinho Rezende | Toninho Geraes
Intérprete: Toninho Geraes
Álbum: Tudo o que sou, Vol. 1: Fragmentos (Ao Vivo)
Ano: 2019
Forma: intro, AA'R AA'R, última estrofe à vontade, "nosso amor..." à vontade
Obs: Intro com laialaia, R' usa laialaiala no primeiro verso das estrofes
Tom: A
Tessitura: D3 - F#4
BPM: 95
Alt: sim
Cifraclub: https://www.cifraclub.com.br/toninho-geraes/se-a-fila-andar/
Catálogo: https://immub.org/album/tudo-que-sou


A    A7

  .       D                 .                   Dm6
% \nobreak \partial 8.
%  a8 e'16~ | \sinc e d e d4 | r4 \sinc r f, e'~ | \sinc e d e d4 |
  .               C#m
% r4 r8. fis16~ | fis8. cis16~ \sinc cis cis cis |
  .                               F#7(13b)   .
% cis8. bis16~ \sinc bis cis d~ | d4  cis4~ | cis4 \sinc r fis, cis'~ |
  B7                    .                     Dm
% \sinc cis b a cis4 | b4 \sinc r fis cis'~ | \sinc cis b a f4 |
  E7                    A
% e4~ e8. b'16~ | b16 a8.~ a4 |

Cº  Bm7  E7

[A]

A                                        B7
Não, eu não estou preparado pra te encontrar amor
                      Bm
Nos braços de outra pessoa
         E7
Só de pensar
            A6  C7(#11)  Bm  Bb7(#11)
Já me causa dor

[A']

A
  Se o sonho acabar, se a fila andar
            B7
não vou aceitar facilmente
               Bm
Tente me preservar
                   E7                A  C#m/G#  Em/G  A7
 o que a gente não vê, o coração não sente

[R]

         D        E/D
Vou te admirar se não for vulgar
C#m                      F#7(13b)
  e não se exibir por castigo
     B7                  Dm        E7
Pois corro o perigo, dos nossos amigos
    A           C#m/G#         Em/G     A7
me virem chorar      não quero nem imaginar

   D             Dm
Se você chegar e me encontrar
C#m                       F#7
no samba eu lhe peço um favor
   B7                Dm         E7
Respeite o insano, sagrado e profano
    A            ( C#m/G# Em/G A7 ) ( E7 )
que foi o nosso amor
