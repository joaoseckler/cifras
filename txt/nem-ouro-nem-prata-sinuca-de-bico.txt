Nem ouro nem prata - José Jorge | Ruy Maurity
Versão: Sinuca de bico
Tom: Em
Forma: intro ABC ABC A intro
Tessitura: B3 - D4
BPM: 100
Alt: sim
Cifraclub: https://www.cifraclub.com.br/teresa-cristina-grupo-semente/nem-ouro-nem-prata/


Am   Em    F#7   B7    Em

[A]

         Em           B7   Em
Eu vi chover, eu vi relampiar
             E7               Am
Mas mesmo assim o céu estava azul
                           Em
Samborê, pemba, folha de Jurema
       F#m7(b5) B7      Em    B7
Oxóssi reina de norte a sul

         Em           B7   Em
Eu vi chover, eu vi relampiar
             E7               Am
Mas mesmo assim o céu estava azul
         Am6               Em
Samborê, pemba, folha de Jurema
       F        B7      Em    F#m7(b5) B7
Oxóssi reina de norte a sul

[B]

Em                                 Am
Sou brasileira, faceira, mestiça mulata
                  Em                  B7                Em   B7
Não tem ouro, nem prata, o  samba que sangra do meu coração
Em            E7   Am            F#m7(b5)
Tua menina de cor, Pedaço de bom carinho
              Em                 B7                   Em    B7
entrei no teu passo Malandra não sou como a tal Conceição

[C]

Em                 E7                 Am    Am6
Chega de tanto exaltar essa tal de saudade
              Em               B7               Em   E7
Meu caboclo moreno, mulato, amuleto do nosso Brasil

Am                F#m7(b5)          Em                 Em/D
Olha, meu preto bonito Te quero, prometo, te gosto pra sempre
         Am                 B7                Em   E7
do samba-canção ao primeiro apito do ano três mil

Am                F#m7(b5)          Em                 Em/D
Olha, meu preto bonito Te quero, prometo, te gosto pra sempre
         Am                 B7                Em   B7
do samba-canção ao primeiro apito do ano três mil
