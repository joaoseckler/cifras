Falsa consideração - Eros Fidélis | Liebert | Marquinhos Satã
Intérprete: Marquinhos Satã
Álbum: Marquinho Satã
Ano: 1986
Tom: D
Forma: intro, AB AB, B à vontade
Obs: Últimos Bs com laialaiá
Tessitura: C3 - F#4
BPM: 100
Cifraclub: https://www.cifraclub.com.br/jorge-aragao/falsa-consideracao/
Catálogo: https://www.discogs.com/release/5624765-Marquinhos-Sat%C3%A3-Marquinhos-Sat%C3%A3


  Em7    A7                 D                       B7
% \nobreak b4 d | cis4 r16 e8 b16~ | b a8 d16~ d e8 fis16~ | fis4 r8. gis,16~ |
  E7                          A7                  D                        A7
% gis16 b8 d16~ d fis8 e16~ | e4  r16 d8 fis16~ | fis e8 d16 cis b8 a16~| a4 a,4

[A]

D                                F#m
Agora eu sei que o amor que você prometeu
                       Am          D7
Não foi igual ao que você me deu
                    G
era mentira o que você jurou
    G            A7/G
Mas não faz mal
                      F#m7         Bm7
eu aprendi que não se deve crer
                     Em
Em tudo aquilo que alguém nos diz
                  C          F
Num momento de prazer ou de amor
    D                                F#m
Mas tudo bem eu sei que um dia vai e outro vem
                       Am          D7
Você ainda há de encontrar alguém
                      G            D7
pra lhe fazer o que você me fez

[B]

  G    A7/G
E ai
                          F#m                Bm7
na hora do sufoco sei você vai me procurar
                                   Em              A7
Com a mesma conversa que um dia me fez apaixonar
                                 Am7  D7
Por alguém de uma falsa consideração
   G    A7/G
e aí         você vai perceber
                  F#m7   B7
Que eu estou numa boa
                                      E7     Em7
Que durante algum tempo fiquei sem ninguém
                A7                  D     ( D7 )
Mas há males na vida que vem para o bem
