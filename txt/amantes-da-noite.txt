Amantes da noite - Dedé Da Portela | Dida
Intérprete: Jorge Aragão
Álbum: Um Jorge
Ano: 1993
Tom: G
Forma: ARR
Tessitura: C3 - Eb4
BPM: 76
Cifraclub: https://www.cifraclub.com.br/jorge-aragao/amantes-da-noite/
Catálogo: https://immub.org/album/um-jorge

Am    D7    Bm     Bbº
Am    D7    G     G#º

[A]

Am     Cm                    Bm
Quando    a noite abre o seu manto
Bb°                  Am
    Sua pureza seu encanto
D7              G°   G
É um mundo de beleza
    Cm    F7              Bb7+
Seu canto    suaviza madrugadas
Db°              D  F°    Em
Pelas ruas e calça__das
A7                Am    D7
Sonorizando a natureza
Am     Cm                      Bm   Bbº
A musa    se encontra com a poesia
                   Am
A letra com a melodia
D7                     Bm5-/7   G7
    A inspiração com a pure_____za
Cm  F7                 Bb7+   D7   Gm
Sua voz em forma de acalan___to
                 Cm
Ameniza qualquer pranto
Am5-/7        D7          Dm5-/7  G7
    Nos dá um toque de certe______za


[R]

i
Cm    F7                Bb7+   D7   Gm
Ai de nós amantes da boemi_____a____a
                        Cm
Se a noite fosse como o dia
  Am5-/7    D7      ( Dm5-/7   G7 )
A vida perderia a grande_______za.

( Gm   Gm5+  Gm6   Gm7   Cm   D7   Gm )
