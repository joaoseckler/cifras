description: Sobre o Laialaiá

# Sobre

[**Porque?**](sobre/porque) uma comparação com o CifraClub, visando
enunciar alguns princípios norteadores deste site
{ .sobre-item }

[**Como contribuir**](sobre/contribuir) como interagir com o
repositório de cifras para pedir cifras, sugerir edições, enviar cifras
ou enviar correções
{ .sobre-item }

[**Como escrever sua cifra**](sobre/escrever) combinados e
possibilidades para escrever sua cifra
{ .sobre-item }

<br />

Códigos fonte: [faixada](https://gitlab.com/joaoseckler/laialaia),
[fundos](https://gitlab.com/joaoseckler/website/-/tree/master/laialaia) e
[repositório de cifras](https://gitlab.com/joaoseckler/cifras)
