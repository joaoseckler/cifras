Brilho no olhar - Almir Guineto | Celso Leão | Daniel Brasileiro
Intérprete: Almir Guineto
Álbum: Pés
Ano: 1997
Forma: ABBRR ABBRR, R à vontade
Tom: D
Tessitura: F#3 - G4
BPM: 67
Cifraclub: https://www.cifraclub.com.br/almir-guineto/brilho-no-olhar/
Catálogo: https://immub.org/album/paulinho-da-viola-3

D7+      Em7     F#m7    Em   A7
D7+      Em7     F#m7    Em   A7

[A]

D7+                             G7+    F#m7
O que aconteceu com aquele sentimento
Em7            A7              D7+ Em7 A7
   Cadê aquele brilho no teu olhar
D7+                                        G7+    F#m7
   Não deixe que o tempo apague a nossa história
Em7               A7              D7+  Am7 D7
   Porquê ainda é tempo pra recomeçar

[B]

G7+   F#7   Bm         A#m7 Am7 D7
Eu só sei dizer que te a____mo
G7+    A    D    Am D7
   Te amo demais

[R]

G7+         A7              F#m7
Vem eu não quero esse jogo louco
     Bm                Em7
Sem você eu fico no sufoco
       A7                 Am7 D7
Senta aqui pra gente conversar
G7+       A7              F#m7
Vem duas vidas numa só estrada
   Bm                    Em7
É amor e não conto de fadas
F#m7  G7+   A7    C  D7(5+)
Eu nasci pra te amar
