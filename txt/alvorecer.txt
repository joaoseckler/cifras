Alvorecer - Délcio Carvalho | Dona Ivone Lara
Intérprete: Dona Ivone Lara | Netinho
Álbum: Bodas de ouro - Dona Ivone Lara
Ano: 1997
Tom: F
Forma: ABAB, último verso 2x, intro
Obs: A é 122
Tessitura: C3 - F4
BPM: 81
Cifraclub: https://www.cifraclub.com.br/dona-ivone-lara/alvorecer/
Catálogo: https://immub.org/album/bodas-de-ouro

  .    G7       C7                 Am7  G#°                   Gm7
% \nobreak \partial 8
% d8 | a4. d8 | c4~ \sinc c f e~ | e2 | r8. g16 \sinc f e d | f4. d8 |
  C7                         F7M          C7(4)
% a4~ \sinc a bes c~ | \sinc c f e d8 c | bes2\fermata


[A1]

F              D7        G7(9)
   Olha como a flor se ascende
         Gm7   C7   F7M
Quando o dia   a__manhece
      A7         Dm7
Minha mágoa se esconde
G7             Gm7   C7
A esperança apare____ce
F           D7     G7(9)
O que me restou da noite
     Gm7  C7        Am7(5-)
O cansaço    a incerteza

[A2]

D7     Gm   Bbm6     F    D7
Lá se vão       na beleza
      G7(9)  C7       ( Am7(5-)   D7 )  ( F   Dm7 )
desse lindo     alvorecer

[B]

Gm              C7                  F
E esse mar em revolta que canta na areia
Em7(5-)             A7                    Dm7
Qual a tristeza que trago em minh'alma campeia
          Gm       C7               F
Quero solução sim,    pois quero cantar
      D7           Gm   C7                  Am7(5-)       D7
Desfrutar dessa alegria que só me faz despertar, do meu penar

Gm             C7           Am7
E esse canto bonito que vem d'alvorada
Em7(5-)              A7              Dm7
    Não é meu grito aflito pela madrugada
           Gm7  C7              Am7
Tudo tão suave,    liberdade em cor
  D7               Gm        C7       F   C7
Refúgio da alma vencida pelo desamor
