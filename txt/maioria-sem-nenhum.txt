Maioria sem nenhum - Mauro Duarte | Elton Medeiros
Intérprete: Elton Medeiros
Álbum: Samba na Madrugada - Paulinho da Viola | Elton Medeiros
Ano: 1966
Tom: Fm
Forma: RR B RR C RR, último verso 3x, intro à vontade
Tessitura: C3 - Db4
BPM: 88
Cifraclub: https://www.cifraclub.com.br/elton-medeiros/a-maioria-sem-nenhum/
Catálogo: https://immub.org/album/samba-na-madrugada-paulinho-da-viola-e-elton-medeiros

% \nobreak \partial 16*3
% c16 c' c, | c' c, c' c, \sinc c' c, c'~ | c4 r16 c,16 c' c, |
% c' c, c' c, \sinc c' c, c'~ | c4 r16 c,16 c' c, | c'4


[R]

        Bbm
Uns com tanto
       C7           Fm
Outros tantos com algum
          Bbm   C     Fm  ( F7 )
Mas a maioria   sem nenhum

[B]

        Fm                     Bbm
Esta história de falar em só fazer o bem
       Fm    Db7                 C7
Não convence quando o efeito não vem
         Fm                        Bbm     Gm7(b5)
Porque somente as palavras não dão solução
       Fm             Db                  C7   Cm7(b5)   F7
Aos problemas de quem vive em tamanha aflição

[C]

         Fm                     Bbm
Há muita gente neste mundo estendendo a mão
     Fm    Db7            C7
Implorando uma migalha de pão
          Fm             F7       Bbm       Gm7(b5)
Eis um conselho pra quem vive por aí a esbanjar
    Fm       Db7             C7   Cm7(b5)   F7
Dividir para todo mundo melhorar
