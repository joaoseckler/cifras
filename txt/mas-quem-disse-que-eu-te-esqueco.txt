Mas quem disse que eu te esqueço - Dona Ivone Lara | Hermínio Bello de Carvalho
Intérprete: Dona Ivone Lara
Álbum: Bodas de Ouro
Ano: 1997
Tom: G
Forma: ABB, intro, ABB, improviso sobre o refrão "Mas quem disse..."
Obs: Na gravação, a segunda repetição da música vai para F maior para o Zeca cantar
Tessitura: B2 - F#4
BPM: 90
Cifraclub: https://www.cifraclub.com.br/dona-ivone-lara/mas-quem-disse-que-eu-te-esqueco/
Catálogo: https://immub.org/album/bodas-de-ouro

    .   G  Em7  A7 . Am7  D7  G6     D7   G   D7
% \nobreak \partial 16 b16 |
% \repeat volta 2 {
% \sinc d e g \sinc e d b | \sinc a g e \sinc g e g | b2 |
% \sinc b a b a8. e16 | b'2 | \sinc b a b d8. b16 |
% \alternative {
%   { e,2 | r4 r8. b'16 }
%   { g2 }
% }
% }
% r2

[A]

    G                   Em7      A7
Tristeza rolou nos meus olhos do jeito que eu não queria
Am                D7         G          D7
E manchou meu coração, que tamanha covardia
 G             E7        A7
Afivelaram meu peito pra eu deixar de te amar
 Am               D7        G     C        G
Acinzentaram minh'alma, mas não secaram o olhar

 G             E7        A7
Afivelaram meu peito pra eu deixar de te amar
 Am               D7        G     C        G
Acinzentaram minh'alma, mas não secaram o olhar

[B]

 G            E7        A7
Saudade amor,    que saudade
                  Am         D7        G      A7   D7
Que me vira pelo avesso, e revira meu avesso
  G            E7     A7
Puseram a faca no meu peito
                           Am
Mas quem disse que eu te esqueço
         D7             G    ( A7   D7 )  ( Am7  D7 )
Mas quem disse que eu mereço
