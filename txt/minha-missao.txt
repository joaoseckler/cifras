Minha missão - João Nogueira | Paulo César Pinheiro
Intérprete: João Nogueira
Álbum: O Homem dos Quarenta
Ano: 1981
Tom: Bm
Forma: ABCD ABCD, coda à vontade
Tessitura: G3 - D5
BPM: 105
Cifraclub: https://www.cifraclub.com.br/joao-nogueira/minha-missao/
Catálogo: https://immub.org/album/o-homem-dos-quarenta

  Em/B                Gm6/Bb                 D7M/A
% \hinc 1.5 \nobreak
% e,4 \sinc fis g b | d4~ \sinc d cis cis~ | cis4 \sinc b fis d |
  Ab7(b5)               G7M  ,  Em7        G7/F ,   F#7         G7M   F#7
% a'4~ \sinc a aes g~ | g4 \sinc fis' e b | d4~ \sinc d cis b~ | b2 | r2

[A]

Bm     G7   C#m7(b5)             Bm
Quando eu   canto,   é para aliviar
G7     F#7                        Bm7  G7         F#7
   Meu pranto, e o pranto de quem já      tanto sofreu
Bm     G7   C#m7(b5)   F#7          Bm7
Quando eu   canto,         estou sentindo a luz
G7       F#7              Bm7    G7     F#7
   De um santo, estou ajoelhando    aos pés de Deus

[B]

B7                    E7(4)   E7
Canto para anunciar o di______a
A7                    D7(4)   D7
Canto para amenizar a noi_____te
G7M                    C#m7(5-)
Canto pra denunciar o açoite
F#7                       Bm7
Canto também contra a tirania

B7                    E7(4)   E7
Canto porque numa melodi______a
A7                   D7(4)    D7
Acendo no coração do po_______vo
G7M                     C#m7(b5)   C7
A esperança de um mundo no_________vo
    Bm7            C#m7(b5)  F#7    Bm    F#7
E a luta para se viver           em paz

[C]

Bm               A                G
Do poder da criação,  sou continuação
  F#7         Bm     F#m7(b5)  B7
e quero agradecer
Em    C#m7(b5)     Bm7
Foi ouvida a minha súplica
C#7               C#m7(5-)   F#7
Mensageiro sou da música

[D]

Bm                   A                   G
O meu canto é uma missão tem força de oração
  F#7          Bm      F#m7(b5)   B7
E cumpro o meu dever

Em            C#m7(b5)   Bm           D7/A   G7
Aos que vivem a       chorar, eu vivo pra cantar
        F#7   Bm    Fº
E canto pra viver
Em            C#m7(b5)   Bm           D7/A   G7
Aos que vivem a       chorar, eu vivo pra cantar
        F#7   Bm    F#7
E canto pra viver

[coda]

Bm        C#m7(b5)   F#7         Bm     G7
Quanto eu canto,   a morte me percorre
      F#7                  Bm7     C7(b5)   Bm7
E eu solto, um canto da garganta
                     Em    G7
Que a cigarra quanto canta morre
F#7                     G     F#7
     E a madeira quando morre canta
