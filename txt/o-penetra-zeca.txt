O Penetra - Zé Roberto
Intérprete: Zeca Pagodinho
Álbum: Zeca Pagodinho ao Vivo
Ano: 2003
Tom: D
Forma: intro AA BB RR AA BB RR intro
Tessitura: D3 - G4
BPM: 100
Cifraclub: https://www.cifraclub.com.br/zeca-pagodinho/o-penetra/
Catálogo: https://immub.org/album/acustico-mtv-zeca-pagodinho


[intro]

  G                   D              A7                    D
% \nobreak
% b'8. g16 fis e d b | \sinc a a' a r4 | r8. g16 fis e cis a | fis r8. r4 |
  G                   D                       A7                  D
% g'16 r8. \sinc r e d | fis16 r8. \sinc r d cis | e8 b'16 a dis, e b a | d,4


     ( A7 )  ( D )
Quem é ele?


[A]

      D
 Não sei quem convidou
     Em
 Sei lá, ninguém soube
  A7                 D
Dizer como fez pra entrar
     A7       D
Já bolado, pensei
                  G
Isso não vai prestar
                D
O cara pagava mico
             A7
Soprava um apito
            D
Cismou de zoar
G                   D
O que é dele tá guardado
              A7
No final da festa
              D    ( A7 )  ( . )
O bicho vai pegar

[B]

        Em
Bebeu demais
         A7             D
Comeu de tudo, dançou sozinho
         Bm            Em
Encheu o bolso de salgadinho
A7                D    Bm
Foi pra fila da pipoca
   Em               A7              D
Roubou um pedaço de bolo e o refrigerante
              Bm               Em
Que estava na mão do aniversariante
      A7         D
Fez a criança chorar

[R]

        A7                               D
Ai, ai, ai, o couro comeu antes mesmo da festa acabar
        A7                   D      D7
Ai, ai, ai, teve que sair na marra
   G     G#º      D
Penetra,     bem-feito
      B7           Em
Foi expulso, ralou peito
        A7         D
Depois de tanto apanhar
