Silêncio no Bexiga - Plínio Marcos
Versão: Sinuca de bico
Forma: A ABAB coda
Obs: Primeiro A é ralentando
Tom: C
Tessitura: G3 - G4
BPM: 75
Alt: sim
Cifraclub: https://www.cifraclub.com.br/beth-carvalho/silencio-no-bexiga/


[A]

C
Silêncio

O Sambista está dormindo

Ele Foi mais foi sorrindo
                 A7           Dm  A7
A noticia chegou quando anoiteceu
Dm  Dm/C# Dm/C                        G7     Dm
Escolas      eu peço silêncio de um minuto
                 G7
O Bexiga está de luto
    Dm          G7           C   C7
O apito de pato D'agua emudeceu

[B]

F      Fm
Partiu
                 G7                    C/E   Em7(b5)  A7
Não tem placa de bronze não fica na história
                              D7
Sambista de rua morre sem glória
                    G7                 C   C7
Depois de tanta alegria que ele nos deu
    F     Fm
E assim
              G7     C/E   Em7(b5)  A7
Um fato se repete de novo
                            D7
Sambista de rua artista do povo
         Fm     G7             C      G7
E é mais um que foi sem dizer adeus

[coda]

C
Silêncio.....Silêncio........Silêncio.
