Coração Leviano - Paulinho da Viola | Ed Saci
Intérprete: Paulinho da Viola - Paulinho da Viola
Álbum: Paulinho da Viola - Paulinho da Viola
Ano: 1978
Tom: E
Forma: 2x inteira, último verso à vontade
Tessitura: B2 - E4
BPM: 110
Cifraclub: https://www.cifraclub.com.br/paulinho-da-viola/coracao-leviano/
Catálogo: https://immub.org/album/paulinho-da-viola-4
E6

[A]

E          Eº         E
Trama em segredo teus planos
            C#7    F#m7   Bm6/D  C#7
Parte sem dizer   adeus
    F#m                  Am7(b5)/C   B7
Nem lembra dos meus desenga__________nos
F#7       F#m7     B7
Fere quem tudo perdeu

[R]

A       A#º     E/B
Ah! Coração leviano
    C#7        F#7   B7   G#m7   Gº
Não sabe o que fez   do   meu
F#m7     Gº      E/G#
Ah!  Coração leviano
    C#7        F#7   B7   E
Não sabe o que fez   do   meu

[B]

F#m      B7        E
    Este pobre navegante
             F#m7  Gm7  G#m7  A7
Meu coração amante
                  G#7
Enfrentou a tempestade
   C#m        B7          E
No mar da paixão e da loucura
 C#m               F#7
Fruto da minha aventura
                  F#m7 B7
Em busca da felicidade

[R']

A       A#º       E/B
Ah! Coração teu engano
A7         D7(9)      C#7(9)
   Foi esperar por um bem
   F#m7   Am6     E/G#
De um coração leviano
    C#7     F#m7   B7    E     ( C#7 )
Que nunca será     de ninguém
