#!/usr/bin/env python3

import subprocess as sp
import sys
from glob import glob
from os.path import getctime
from urllib.parse import quote_plus


def get_fname():
    if len(sys.argv) == 1:
        list_of_files = glob("txt/*")
        latest_file = max(list_of_files, key=getctime)
        print(f"usando {latest_file}...")

        return latest_file

    return sys.argv[1]


def get_search():
    if len(sys.argv) > 2:
        return " ".join(sys.argv[1:])

    fname = get_fname()
    with open(fname, "r") as f:
        lines = f.readlines()[:2]
    name = lines[0].split(" - ")[0].strip()
    interp = lines[1].split(":", maxsplit=1)[1].strip()

    return f"{name} {interp}"


def main():
    search = quote_plus(get_search())
    urls = [
        f"https://www.youtube.com/results?search_query={search}",
        f"https://www.cifraclub.com.br/?q={search}",
        f"https://www.google.com/search?q={search}+immub",
    ]

    for url in urls:
        sp.run(["xdg-open", url])


if __name__ == "__main__":
    main()
