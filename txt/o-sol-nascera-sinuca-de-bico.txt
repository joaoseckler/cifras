O sol nascerá - Cartola | Elton medeiros
Versão: Sinuca de bico
Obs: Pronuncia-se "naiscerá"
Forma: intro, 1x inteira, 1ª estrofe à vontade
Tom: A
Tessitura: A2 - A3
Alt: sim
BPM: 102
Cifraclub: https://www.cifraclub.com.br/cartola/o-sol-nascera/


A7    D    Dm6    A    E7

A   A7   D
A   sor__rir
              B7    Bm7  E7
Eu pretendo levar a vi___da
A    A7  D
Pois chorando
            B7    E7    A    E7
Eu vi a mocidade     perdida

A A7   D
A sor__rir
              B7    Bm7  E7
Eu pretendo levar a vi___da
A    A7  D
Pois chorando
            B7    E7    A    E7
Eu vi a mocidade     perdida

Em7           A7
Finda a tempestade
D7M
O sol nascerá
Dm6
Finda esta saudade
       A           B7         E7
Hei de ter outro alguém para amar
