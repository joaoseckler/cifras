Encanto da paisagem - Nelson Sargento
Intérprete: Nelson Sargento
Álbum: Encanto da paisagem
Ano: 1986
Tom: F
Forma: ABR, último acorde à vontade
Tessitura: F3 - C5
BPM: 94
Cifraclub: https://www.cifraclub.com.br/nelson-sargento/encanto-da-paisagem/
Catálogo: https://immub.org/album/encanto-da-paisagem-serie-grandes-sambistas
Youtube: https://www.youtube.com/watch?v=XnUEgjJ4f04

F7M

[A]

F7M                       Bb7   A7
Morro, és o encanto da paisa____gem
              Am7(b5)   D7
Suntuoso personagem
                Gm
de rudimentar beleza
C7                          F     F7
Morro, progresso lento e primário
Bb7M                 Bm7(b5)   E7
   És imponente no cená________rio
A7                   Am7(b5)   D7
   Inspiração da nature________za
Gm7   Bbm                 Am7
Na        topografia da cidade
G#º                 Gm7
   Com toda simplicidade
      C7           Cm7   F7
és chamado de elevação
  Bb7M   Eb7           Am7    Dm
Vielas,      becos e buracos
                         Gm7
Choupanas, tendinhas, barracos
C7            F7M     C7
Sem discriminação

[B]

F7M                       G7
Morro, pés descalços na ladeira
                 Gm
Lata d´água na cabeça
     C7          F7M    C7
Vida rude alvissareira
   F7M         G7           C
Crianças sem futuro e sem escola
       Am           Dm
Se não der sorte na bola
      G7            Gm    C7
Vai sofrer a vida inteira

[R]

F7M                      G7
Morro, o teu samba foi minado
                 C7                     Cm7   F7
Ficou tão sofisticado, já não é tradicional

Bb7M   Bbm                          Am7    D7
Morro,     és lindo quando o sol desponta
                                          ( Cm7   F7  )
                     Gm7          C7      ( Fº        )
E as mazelas vão por conta do desajuste social
