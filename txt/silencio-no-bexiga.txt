Silêncio no Bexiga - Plínio Marcos
Intérprete: Plínio Marcos
Álbum: Em prosa e samba
Ano: 2012
Forma: A ABAB coda
Obs: Primeiro A é ralentando
Tom: F
Tessitura: C3 - C4
BPM: 75
Cifraclub: https://www.cifraclub.com.br/beth-carvalho/silencio-no-bexiga/
Catálogo: https://immub.org/album/plinio-marcos-em-prosa-e-samba-nas-quebradas-do-mundareu-geraldo-filme-zeca-da-casa-verde-e-toniquinho-batuqueiro


[A]

F
Silêncio

O Sambista está dormindo

Ele Foi mais foi sorrindo
                 D7           Gm  D7
A noticia chegou quando anoiteceu
Gm  Gm/F# Gm/F                        C7     Gm
Escolas      eu peço silêncio de um minuto
                 C7
O Bexiga está de luto
    Gm          C7           F   F7
O apito de pato D'agua emudeceu

[B]

Bb     Bbm
Partiu
                 C7                    F/A   Am7(b5) D7
Não tem placa de bronze não fica na história
                              G7
Sambista de rua morre sem glória
                    C7                 F   F7
Depois de tanta alegria que ele nos deu
    Bb    Bbm
E assim
              C7     F/A   Am7(b5)  D7
Um fato se repete de novo
                            G7
Sambista de rua artista do povo
         Bbm    C7             F      C7
E é mais um que foi sem dizer adeus

[coda]

F
Silêncio.....Silêncio........Silêncio.
