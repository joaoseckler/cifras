Foi um rio que passou em minha vida - Paulinho da Viola
Intérprete: Paulinho da Viola
Álbum: Foi um rio que passou em minha vida
Ano: 1970
Forma: 1x inteira, últimos 2 versos à vontade
Obs: Na versão original não tem lalaiá no final
Tom: A
Tessitura: G#2 - E4
BPM: 112
Cifraclub: https://www.cifraclub.com.br/paulinho-da-viola/foi-um-rio-que-passou-em-minha-vida/
Catálogo: https://immub.org/album/foi-um-rio-que-passou-em-minha-vida



A ...

A
Se um dia
        F#7           Bm
Meu coração for consultado
                      E7
Para saber se andou errado
               A    E7  A
Será difícil negar
        E7                 A
Meu coração tem manias de amor
           F#7       Bm
Amor não é fácil de achar
  Dm                  D#º
A marca dos meus desenganos
  A      F#7
Ficou, ficou
Bm       E7          A   A7
  Só um amor pode apagar

  D                   D#º
A marca dos meus desenganos
  A      F#7
Ficou, ficou
Bm       E7          A  E7
  Só um amor pode apagar

  A    E7    A
Porém, ah! Porém
      E7       A
Há um caso diferente
       C#7          F#m7
Que marcou um breve tempo
         Bm
Meu coração para sempre
E7         A         A/C#
Era dia de carnaval
        E7           A
Eu carregava uma tristeza

Não pensava em novo amor
                         F#7
Quando alguém que não me lembro
      Bm
Anunciou

Portela! Portela!
                     E7
O samba trazendo alvorada
Bm      E7         A    E7
Meu coração conquistou
A  F#7         Bm
Ah!   Minha Portela
E7                  A   E7
  Quando vi você passar
   A          F#7      Bm
Senti meu coração apressado
                 E7
Todo meu corpo tomado
                 A   E7
Minha alegria voltar
    A
Não posso definir aquele azul
           F#7             Bm
Não era do céu, nem era do mar
D        C#7        F#m          F#7
  Foi um rio que passou em minha vida
Bm           E7        A         A7
   E meu coração se deixou levar
D        C#7        F#m          F#7
  Foi um rio que passou em minha vida
Bm           E7        A     A7
   E meu coração se deixou levar

   D  D#º  A  F#7
La iá ...
   Bm  E7  A A7
La iá ...
   D  D#º  A  F#7
La iá ...
   Bm  E7  A
La iá ...
