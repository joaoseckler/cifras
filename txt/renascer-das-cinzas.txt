Renascer das cinzas - Martinho da vila
Intérprete: Martinho da Vila
Álbum: Martinho da Vila
Ano: 1974
Forma: duas vezes inteira
Tom: Am
BPM: 93
Tessitura: E2 - E4, G#2 - A4
Cifraclub: https://www.cifraclub.com.br/martinho-da-vila/vamos-renascer-das-cinzas/
Catálogo: https://www.discogs.com/master/372005-Martinho-Da-Vila-Canta-Canta-Minha-Gente


Am        A7       Dm
Vamos renascer das cinzas
Bm7(5-)           E7  Am   E7
Plantar de novo o arvoredo
Am            A7     Dm
Bom calor nas mãos unidas
Bm7(5-)       E7       Am   E7
Na cabeça  um grande enredo
Am         A7 Dm
Ala de compositores
   Bm7(5-)       E7    Am    E7
Mandando o samba no terreiro

            Am              A7                Dm
Cabrocha sambando, cuíca roncando, viola e pandeiro
           Bm7(5-)           E7                   Am       E7
No meio da quadra, pela madrugada, um senhor partideiro

              Am            A7                 Dm
Sambar na avenida de azul e branco é o nosso papel
              Bm7(5-)             E7                 Am   E7
Mostrando pro povo que o berço do samba é em Vila Isabel
              Am            A7                 Dm
Sambar na avenida de azul e branco é o nosso papel
              Bm7(5-)             E7                 Am   E7
Mostrando pro povo que o berço do samba é em Vila Isabel

Am A7   Dm
Tão  bonita a nossa escola!
Bm7(5-) E7  Am         E7
E é     tão bom cantarolar
Am  A7  Dm
La, la, iá, iálaraia
Bm7(5-)  E7 Am ( E7 )
la       la iá
