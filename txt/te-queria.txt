Te queria - Flavio Tris
Intérprete: Flavio Tris
Álbum: Vela
Ano: 2021
Tom: C
Forma: Última estrofe de novo pra acabar
Tessitura: G3 - A4
BPM: 115
Youtube: https://www.youtube.com/watch?v=bcjnDHybmoY

C6(9)

[A]

        C6(9)    G/B
Eu te queria
          Am     Am/G
Te queria perto
          G#°
Te queria sempre
                B°   Am   Fm/Ab
Para ouvir meus pas__sos

             C      F/C
Te queria ao lado
             C7     F
Pra andar no mato
          Bb
Te queria livre
            Ab      G
Dos teus percalços

[B]

          C       G/B
Te queria rindo
            Am    Am/G
Na graça do dia
          G#°
Te queria fundo
      B°    Am    Fm/Ab
Na gruta da noite

               C/G   G#°
Te queria no escuro
             Am     Fm/Ab
Te queria no claro
             C/G    F#7
No café e na janta
            F7M        Fm6
Te queria calado e sem pressa

[C]

              C6(9)    G/B
Entender teus olhos
               Am     Am/G
Teus esses nos ossos
              G#°
Te queria num mundo
   B°   Am   Fm/Ab
Só   nosso

               C      F/C
Descobrir os humores
                C7
E os sabores da língua
                   F7M
Queria não viver a míngua
             G7(sus4)   G7(9b)
Na noite que finda
            C
No fim do verão


C    G/B    Am     Am/G
G#°  B°     Am     Fm/Ab

               C      F/C
Descobrir os humores
                C7
E os sabores da língua
                   F7M
Queria não viver a míngua
             G7(sus4)   G7(9b)
Na noite que finda
            C
No fim do verão
