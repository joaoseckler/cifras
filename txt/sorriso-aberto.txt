Sorriso aberto - Guará
Intérprete: Jovelina Pérola Negra
Álbum: Sorriso Aberto
Ano: 1988
Forma: 2x inteira, A à vontade
Tom: Dm
Tessitura: A3 - D5
BPM: 110
Cifraclub: https://www.cifraclub.com.br/jovelina-perola-negra/sorriso-aberto/
Catálogo: https://immub.org/album/sorriso-aberto


  Em7(5b)  A7              Dm             Dm/C                     A7
% \nobreak
% g4 d' |   a~ 16 g8 f16~ | f g8 a16~ a4 | r16 d8 a16~ a f8 d16~ | d2     |
  .                         Em7       A7
% g16 f8 g16~ g gis8 a16~ | a4 g8 f | e2

[A]

Dm         C
Lá laiá laiá laiá
       Bb7
Laiá laiá laiá
       A7
Laiá laiá laiá
Dm             C
Lá laiá laiá laiá laiá laiá
       Bb7
Laiá laiá laiá
       A7
Laiá laiá laiá
Dm           A7        Dm                   Em7(5b)       A7
   Lá laiá laiá laiá laiá (samba aqui,samba ali, samba la)
Dm           Em7(5b)   F    A7
   Lá laiá laiá laiá laiá
Dm           A7        Dm       A7
   Lá laiá laiá laiá laiá, pois é...

[B]

Dm              C
É, foi ruim à beça
              Bb7
Mas pensei depressa
         Em7(5b)          A7             Dm
Numa solução para a depressão, fui ao violão
            C
Fiz alguns acordes
            Bb7
Mas pela desordem
         Em7(b5)              A7
Do meu coração, não foi mole não

Em7(b5)         A7       Dm    Dm/C
   Quase que sofri desilusão
Bb              C7         F      C7
   Quase que sofri desilusão (tristeza)

[C]

F
Tristeza foi assim
          C            C#º
Se aproveitando pra tentar
    Dm              Gm
Se aproximar, ai de mim
                  Em7(5b)      A7     Dm
Se não fosse o pandeiro e o ganzá e o tamborim
                Gm  A7       Dm
Pra ajudar a marcar,    meu tamborim
                Gm   A7
Pra ajudar a marcar

[B']

Dm                  C
Logo, eu com meu sorriso aberto
        Bb7                  Em7(b5)  A7
E o paraíso perto, pra vida melhorar
  Dm                      C
Malandro desse tipo que balança mas não cai
            Bb7
De qualquer jeito vai
          Em7(b5)  A7
Ficar bem mais   legal
    Em7(5b)   A7             Dm     Bb7 A7
Pra nivelar a vida em alto astral

    Dm        Em7(5b)        F     A7
Pra nivelar a vida em alto astral
    Dm        A7             Dm                       A7
Pra nivelar a vida em alto astral  (samba aqui, samba ali, samba la)
