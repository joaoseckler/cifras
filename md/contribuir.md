description: Tutorial de como adicionar ou corrigir uma cifra

# Como contribuir

Se você tem alguma correção, sugestão ou quer adicionar ou pedir uma
nova cifra, leia essa página. Qualquer adição, sugestão ou correção é
muito bem vinda, mesmo que pequena.

Essa página ensina como submeter sua cifra para o site, depois que ela
já está escrita ou corrigida. Para ver como escrever cifas para o site,
venha [aqui](escrever).

## Gitlab

As cifras (e código do site) ficam no Gitlab:
[gitlab.com/joaoseckler/cifras](https://gitlab.com/joaoseckler/cifras).
Para interagir por lá, você vai precisar
[criar uma conta](https://gitlab.com/users/sign_up).

Se você já mexe com git, basta abrir uma Issue ou fazer um MR (as cifras
ficam na pasta `txt`).

### Pedir uma cifra

1. Vá para a página de [Issues](https://gitlab.com/joaoseckler/cifras/-/issues)
   do repositório;
2. Clique em "New Issue" (sim, "issue" quer dizer problema em inglês,
   mas não tem problema nenhum pedir uma cifra, muito pelo contrário);
3. Escreva no título que é um pedido e coloque o nome da cifra. Um
   título poderia ser: `[pedido] Volta da sorte`
4. Na descrição, forneça mais detalhes: compositores, intérpretes, qual
   versão está sendo pedida, links para o youtube, spotify, etc.

### Pedido de correção ou melhoria

1. Vá para a página de [Issues](https://gitlab.com/joaoseckler/cifras/-/issues)
   do repositório;
2. Clique em "New Issue" (sim, "issue" quer dizer problema em inglês,
   mas não tem problema nenhum pedir uma correção, muito pelo
   contrário);
3. Escreva no título se é um pedido de correção e coloque o nome da
   cifra. Um título poderia ser: `[correção] Volta da sorte`, ou então
   `[sugestão] Volta da sorte`.
4. Na descrição, forneça mais detalhes: o que está errado, o que poderia
   melhorar, algum raciocínio, etc

Você também pode alterar você mesmo a cifra ao invés de fazer um pedido:
leia a seção [Sugestão/Correção para cifra
existente](#sugestaocorrecao-para-cifra-existente).

### Enviar nova cifra

1. Vá para a
   [pasta de cifras do projeto](https://gitlab.com/joaoseckler/cifras/-/tree/main/txt);
2. Procure por um símbolo de mais (`+`) logo acima da lista de arquivos;
3. Clique na opção "New file" (aqui, talvez o site te interrompa para
   criar um "fork" do repositório: é assim mesmo);
4. Insira sua cifra;
5. Insira o nome do arquivo. Ele deve ser todo em letras minúsculas, sem
   caracteres especiais (acentos, pontuação, etc), com palavras
   separadas por hífen (`-`) e terminando com `.txt`.
5. No campo logo abaixo, descreva brevemente o que você está fazendo
   (algo como: "adiciona cifra para Volta da sorte");
6. Continue lendo a seção "[Fazendo o merge request](#fazendo-o-merge-request)".

### Sugestão/Correção para cifra existente

1. Vá para a
   [pasta de cifras do projeto](https://gitlab.com/joaoseckler/cifras/-/tree/main/txt)
   e clique no arquivo desejado;
2. À direita do nome do arquivo, clique no botão "Edit";
3. Selecione "Edit single file" (aqui, talvez o site te interrompa para
   criar um "fork" do repositório: é assim mesmo);
4. Faça suas correções/sugestões no arquivo;
5. No campo logo abaixo, descreva brevemente o que você está fazendo
   (algo como: "corrige acordes do B de Volta da sorte");
6. Continue lendo a seção abaixo.

### Fazendo o merge request

1. Clique em "Commit changes";
2. Você vai ser redirecionado para uma página "New merge request". Se
   quiser, pode fazer alguma descrição adicional aqui, mas não precisa.
3. Clique em "Create merge request";
4. Pronto! Lembre-se de ler o [guia de escrita de cifras](escrever)
   antes de enviar.

### Adicionando uma nova versão de uma cifra existente

Siga os mesmo passos da criação de nova cifra, mas atentando-se ao
seguinte:

* Recomenda-se que o início do nome do arquivo seja igual ao da cifra
  original. Por exemplo, em "O penetra", consideramos que a cifra
  principal é a da versão do Zé Roberto. O nome do arquivo aqui é
  `o-penetra.txt`. Se quisermos incluir a versão do Zeca Pagodinho,
  usaríamo o título `o-penetra-zeca.txt`.

* Inclua os seguinte campo no cabeçalho:

          Alt: sim
