Argumento - Paulinho da Viola
Intérprete: Paulinho da Viola
Álbum: Paulinho Da Viola
Ano: 1975
Tom: D
Forma: AA B AA B AA, intro à vontade
Tessitura: B3 - E5
BPM: 115
Cifraclub: https://www.cifraclub.com.br/paulinho-da-viola/argumento/
Catálogo: https://immub.org/album/paulinho-da-viola-3

D ...   D7

G                                       G#º                       D/A
% \nobreak
% \sinc b cis d~ \sinc d e cis~ | \sinc cis d b~ \sinc b d cis~ | cis4 \sinc r b cis |
  B7                        Em                      A7                      D
% b8. a16 \sinc g fis b,~ | b4 \sinc r d' b | \sinc cis a g~ \sinc g e d~ | d4


     A7
Tá legal!


[A]

      D        A7          D
Tá legal, eu aceito o argumento
                   B7             Em  B7 Em
Mas não me altere o samba tanto assim
                A7            D          B7
Olha que a rapaziada está sentindo a falta
         Em             A7                  D   ( A7 )  ( . )
De um cavaco, de um pandeiro ou de um tamborim.

[B]

           Em         A7        D
Sem preconceito ou mania de passado
                     Am7          D7          G
Sem querer ficar do lado de quem não quer navegar
              G#º      F#m
Faça como um velho marinheiro
       B7       Em             A7        Am7   D7   G
Que durante o nevoeiro leva o barco devagar
              G#º      F#m
Faça como um velho marinheiro
       B7       Em             A7        D        A7
Que durante o nevoeiro leva o barco devagar. Tá legal!
