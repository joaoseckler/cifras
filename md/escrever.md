description: Combinados de formatação e conteúdo das cifras deste site

# Como escrever sua cifra

As cifras são escritas num arquivo de texto (aquele tipo que abre no
"bloco de notas"). Basta inserir um cabeçalho, como descrito abaixo, e
em seguida escrever sua cifra.

## Cabeçalho

Além da própria cifra, você deverá incluir um cabeçalho no topo do
arquivo. Deve ser mais ou menos assim:

```txt
A batucada de nossos tantãs - Sereno | Adilson Gavião | Robson Guimarães
Intérprete: Fundo de quintal | Dudu Nobre
Álbum: Ao vivo convida - Fundo de Quintal
Ano: 2004
Tom: F
Forma: A1A2R, A1A2R, R à vontade
Obs: último R é laialaiá
Tessitura: D3 - G4
BPM: 120
Cifraclub: https://www.cifraclub.com.br/fundo-de-quintal/a-batucada-dos-nossos-tantas/
Catálogo: https://immub.org/album/fundo-de-quintal-ao-vivo-convida

```

Algumas observações:

*   Repare que tanto a primeira linha quanto o campo "Álbum" seguem o
    seguinte padrão:

        Título - Compostor 2 | compositor 2 | ...

    Assim como o campo "Intérprete" ou "Intérpretes" segue o formato

        Intérprete 1 | Intérprete2 | ...

* O campo "Forma" usa nomes que se referem às partes ou seções da música
  que estão descritas na cifra. Mais detalhes [abaixo](#secoes);

* O campo "Obs" traz alguma observação, geralmente a respeito da forma
  da música. Na página web, o texto "Obs:" não aparece, só o valor do
  campo. *(Observação sobre a observação: em "A batucada de nossos
  tantãs", o último refrão não é de fato em "laialaiá", trata-se somente
  de um exemplo)*

* O campo **Cifraclub** contém um link para uma cifra dessa música
  no [CifraClub](https://www.cifraclub.com.br). Se ela não existe, é uma
  boa oportunidade para enviar a cifra para lá também! Mas não se
  esqueça de fazer as [adaptações necessárias](https://suporte.cifraclub.com.br/pt-BR/support/solutions/articles/64000236814-conheca-o-padr%C3%A3o-para-envio-de-cifras-e-tablaturas)
  para a plataforma deles;

* O campo **Catálogo** contém um link para uma página em que o fonograma
  em questão (faixa ou álbum) esteja catalogado, ou seja, em que estejam
  listadas informações de sua ficha técnica. Recomendamos para isso o
  site do [Instituto Memória Musical Brasileira](https://immub.org)
  ou o [Discogs](https://discogs.com/). Se sua cifra não se baseia em
  nenhum fonograma específico, provavelmente ela devia. Sobre isso, veja
  [essa discussão](porque#versoes);

*  Para medir o BPM da versão que você está usando recomenda-se o site
  [Tap BPM](https://www.beatsperminuteonline.com/);

* Você pode usar `*itálico*`, `**negrito**` e `[links](google.com)` no
  texto dos campos usando sintaxe de Markdown.

Não precisa incluir todas essas informações, mas encoraja-se que se sim.
Os campos listados nesse exemplo são considerados "essenciais": se você
não os incluir, a moderação do site vai. Você pode incluir mais campos,
se quiser, e eles vão aparecer na página web.

## Combinados

### Alinhe acordes com a sílaba em que ele cai

Os acordes caem exatamente sobre a primeira letra da sílaba em que eles
soam. Exemplo:

```txt
   F#m            B7
No bar não vou passar
```

Se o acorde cai num momento em que não há letra, algumas coisas podem
acontecer

* Se o acorde faz parte do fim de uma frase musical (é bem comum uma
  frase durar 4 compassos), coloque o acorde ao fim da linha:

```
                                 Cm7   F7
Que nasce da alma, sem pele, sem cor
```

* Se o acorde faz parte do começo de uma frase musical, coloque no
  início da linha e adicione espaços antes da letra:

```
Gm7         A7            Dm              Dm/C
    Eu só errei quando juntei minh'alma à sua
```

* Se o acorde ocorre no meio da frase, adicione espaços depois da
  palavra anterior e antes da próxima palavra para deixar claro que o
  acorde está entre eles.

```txt
Dm/F       Am   B7      E7    Am
Deus me perdoe      mas vou dizer
```

Para tudo isso funcionar bem, é importante editar a cifra com uma fonte
[monoespaçada](https://pt.wikipedia.org/wiki/Fonte_monoespa%C3%A7ada).

### Dê espaço entre os acordes

Busque nunca usar menos que 3 espaço entre dois acordes. **Não** faça
assim:

```txt
                                 Cm7 F7
Que nasce da alma, sem pele, sem cor
```

Faça assim:

```txt
                                 Cm7   F7
Que nasce da alma, sem pele, sem cor
```

Se a mudança de acordes for durante uma palavra curta, insira tantos
traços baixos (_underscores_) no meio da palavra quanto forem
necessários:


```txt
              D7    Dm7   G7
Eu pretendo levar a vi____da
```

### Formatando acordes sem letra

Eles são comuns em introduções ou passagens instrumentais. Não apinhe
todos no canto esquerdo: dê um respiro entre eles (pelo menos quatro
espaços) e procure quebrá-los em linhas. Geralmente cada linha tem
quatro compassos, mas isso pode variar de música para música. Alinhe os
acordes da linha de baixo com a linha de cima. Resumindo, **não** faça
assim:

```txt
Am7(b5) D7 Gm7 Gm7/F Dm E7 A7 Dm A7
```

Faça assim:

```txt
Am7(b5)    D7     Gm7     Gm7/F
Dm         E7     A7      Dm      A7
```

### Seções

Encorajamos que as cifras contenham informações de seções (refrão, A, B,
etc). Coloque o nome dela entre colchetes, e use pelo menos uma linha em
branco acima e abaixo. O exemplo abaixo mostra a passagem para a seção B
da música.

```txt
Gm7        A7            Dm              Dm/C
   Eu só errei quando juntei minh'alma à sua
   Em7(b5)/Bb         A7           Dm    ( A7 ) ( D7 )
O  sol  não  pode  viver perto da lua

[B]

A7                    Dm
  É no espelho que eu vejo a minha mágoa
        D7            Gm
E minha dor e os meus olhos rasos d'água
```

Use nomes simples para as seções. Procure usar o seguinte:

* R para refrão
* A, B, C... para seções (ou "versos"). Se houver duas seções em que a
  música se repete mas não a prosódia, use A1, A2, B1, B2, etc.
* intro e coda quando aplicável.

Além de guiar o músico pela estrutura da música, essas seções devem ser
usadas no campo "Forma" do cabeçalho.

### Casa um, casa dois

Como discutido [aqui](porque#sucintez), as cifras nesse site buscam
evitar repetição. Para viabilizar isso, adotamos uma notação para
expressar as [_voltas_](https://musicad.com/prima_volta) (ou ["casa um"
e "casa
dois"](https://conversandosobremusica7t2cp2.blogspot.com/2015/05/sinal-de-repeticao-casa-de-1-e-2-vez.html)).
É a seguinte:

```txt
                 ( B7(9) )
B7       E       ( E7    )
```

Nesse exemplo, a casa um é B7(9) e a casa dois é E7 (essa configuração é
bem comum: da primeira vez, ela pede a dominante para repetir essa
seção; da segunda, pede a dominante do quarto grau para preparar a
próxima seção). Note que:

1. Há espaços entre os parênteses e os acordes
2. Os parênteses se alinham perfeitamente uns sobre os outros
3. O início dos acordes se alinham uns sobre os outros

Se algum desses pontos não for observado, as _voltas_ não serão
interpretadas corretamente pelo site, o que vai prejudicar um pouco a
quebra de linhas dessa parte.

Se alguma das casas consistir na repetição do acorde anterior, podemos
usar um ponto (`.`):

```txt
                 ( B7 )
B7       Em      ( .  )
```

Aqui, a casa um é B7 e a casa dois é Em.

Se a música acaba antes de ocorrer a casa dois, omite-se o segundo
parênteses:

```txt
B7       Em      ( B7 )
```

Aqui, a casa um é B7 e a casa dois não existe.

### Fidelidade e detalhes

Queremos que as cifras sejam tão fiéis quanto possível _à versão_ que
ela pretende traduzir. No entanto, o critério de sucintez é mais
importante: ela nunca vai ter duas seções escritas por extenso que são
quase iguais, a não ser por uma pequena diferença de acordes. Em outras
palavras, a cifra deve ser fiel à musica até o nível de detalhe que dá
conta da música como um todo.

Talvez ajude pensar o seguinte: se os músicos que executam a versão
fossem cifrá-la, como o fariam?

## Escrevendo partituras

Para escrever partituras no meio da cifra, inicia a linha com o
caractere `%`, e escreva expressões [LilyPond](https://lilypond.org).
Várias linhas seguidas que se iniciam com `%` resultam numa só linha de
partitura. Se houverem acordes imediatamente acima da linha de
partitura, esses acordes vão se alinhar aos compassos dela.

Se houverem mais linhas de acordes e de partitura alternadas, os acordes
também vão se alinhar aos compassos e a partitura também vai ser
avaliada numa linha só: a única diferença é que uma quebra de linha vai
ser inserida no momento correspondente à quebra da linha dos acordes
(mas veja a opção `\nobreak` abaixo para evitar esse comportamento).

### Alinhando acordes e compassos

Use um ponto (`.`) para indicar uma repetição (ou omissão) de acorde num
determinado momento. Use uma vírgula (`,`) para indicar que o próximo
acorde deveria estar no mesmo compasso que o anterior. Essas duas
operações podem ser combinadas para criar ritmos mais complexos dentro
de um mesmo compasso.

#### Exemplo

O seguinte trecho:

```
           .          C6          Dm , G7   C
% \time 4/4 s2 f,4 d | c4 d e c | f4 g a b | c1
```

resulta em:

<div class="cifra-document">
<div class="anchored-sheet-music line"><span class="pos"><span class="chord" style="left:12.0px;"> </span><svg class="sheet-music-svg" height="38pt" viewBox="0.0 0.0 95.3 38.0" width="95.3pt" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><path d="M4.344-2.703C2.25-2.703 0-.843 0 .937c0 1.047.844 1.766 2.156 1.766C4.25 2.703 6.5.843 6.5-.937c0-1.047-.844-1.766-2.156-1.766m0 0" fill="currentColor" id="a00"></path><path d="M4.25-2.234c.89 0 2.656.687 2.656 2.718 0 1.063-.468 1.75-1.375 1.75-.89 0-2.656-.687-2.656-2.718 0-1.063.469-1.75 1.375-1.75m.625-.47C2.015-2.703 0-1.421 0 0c0 1.422 2.016 2.703 4.875 2.703C7.75 2.703 9.781 1.422 9.781 0S7.75-2.703 4.875-2.703m0 0" fill="currentColor" id="b00"></path><path d="M-.14-7.469 0 3.891c0 .187.156.328.36.328h.062l2.11-.39-.048 3.64c.172.078.329.14.485.14.156 0 .344-.062.5-.14l-.14-11.36c0-.187-.157-.328-.36-.328h-.063l-2.11.39.048-3.64c-.172-.078-.328-.14-.485-.14-.156 0-.343.062-.5.14m2.75 5.235L2.546 1.89l-1.828.343L.78-1.89Zm0 0" fill="currentColor" id="c00"></path><path d="M4.984-5.219C.36-5.219 0-1.234 0 0c0 4.86 3.813 5.234 4.984 5.234 2.641 0 3.641-3.015 3.641-3.468 0-.157-.14-.235-.266-.235-.093 0-.187.063-.234.203-.375 1.61-1.547 3-3.14 3-2.11 0-2.25-1.468-2.25-3.718v-2.032c0-2.25.14-3.703 2.25-3.703a3 3 0 0 1 2.39 1.172c-.11-.015-.203-.015-.281-.015-.89 0-1.438.78-1.438 1.53 0 1 .875 1.5 1.5 1.5.735 0 1.328-.64 1.328-1.437 0-1.14-1.187-3.25-3.5-3.25m0 0" fill="currentColor" id="d00"></path><path d="M.531-.812C.562-1.47.97-2.078 1.594-2.078c.578 0 .86.687.86 1.344C2.453.438 1.5 1.28.593 2.03A9.248 9.248 0 0 1 .516.72V.5ZM-.297 2.75c0 .281.203.5.453.5.422 0 .594-.5.907-.766C2.266 1.454 3.984.72 3.984-.875c0-1.078-.734-2.031-1.765-2.031-.672 0-1.328.25-1.828.672L.53-9.11A1.1 1.1 0 0 0 0-9.266a1.1 1.1 0 0 0-.531.157Zm0 0" fill="currentColor" id="e00"></path><path d="m5.297 12.656.016-.86h-.079c-.968 0-1.812-.515-2.328-1.296.203.078.406.125.625.125 1.031 0 1.89-.86 1.89-1.906 0-1.11-.812-2.047-1.89-2.047-1.187 0-2.234.89-2.234 2.047 0 2.172 1.734 3.937 3.875 3.937Zm3.89-8.61c-.25-2.234-.828-5.28-1.203-7.062 2.141.235 3.454 2 3.454 3.797 0 1.469-.907 2.782-2.25 3.266M1.47-.561c0-3.188 2.11-5.61 4.25-8.032a72.26 72.26 0 0 1 .922 3.5C4.61-4.64 3.454-2.921 3.454-1.171c0 2.484 2.203 3.719 2.5 3.719.25 0 .438-.219.438-.438a.379.379 0 0 0-.141-.296C5.14 1.172 4.64.234 4.64-.642c0-1.156.891-2.203 2.47-2.375C7.796.204 8.233 2.97 8.36 4.25c-.688.11-1.61.11-1.626.11-2.453 0-5.265-2.032-5.265-4.922m3.515-10.454C2.656-8.328.047-5.75.047-2.234c0 3.875 2.781 7.453 6.625 7.453.89 0 1.672-.14 1.781-.157.047.61.078 1.22.078 1.829 0 .359-.031.734-.047 1.093-.125 1.97-1.312 3.766-3.171 3.813l-.016.86c2.266-.063 3.89-2.173 4.031-4.548.016-.375.031-.78.031-1.156 0-.703-.03-1.39-.093-2.094a5.145 5.145 0 0 0 3.578-4.906c0-2.64-2-5.187-5.094-5.187-.125 0-.219.015-.266.015a64.317 64.317 0 0 0-1.109-4.125c1.875-2.172 3.578-4.406 3.578-7.25 0-2.25-1.281-5.547-2.64-7.14a.329.329 0 0 0-.407 0 7.239 7.239 0 0 0-2.968 5.828c0 3.5.468 4.984 1.046 6.89m3.547-7.234c0 2.516-1.312 4.531-2.906 6.453-.484-1.578-.719-2.828-.719-3.86a5.736 5.736 0 0 1 3.219-5.155c.422 1.25.406 1.89.406 2.562m0 0" fill="currentColor" id="f00"></path><path d="M0-2.375v1.203c0 .219.172.39.39.39.047 0 .11 0 .141-.015l.64-.219v3.188l-.905.312A.403.403 0 0 0 0 2.875v1.188a.4.4 0 0 0 .39.406c.047 0 .11 0 .141-.032l.64-.234v2.89c0 .204.157.376.36.376a.38.38 0 0 0 .375-.375V3.937l1.657-.609v2.89a.39.39 0 0 0 .374.376c.204 0 .36-.172.36-.375V3.062l.922-.328a.4.4 0 0 0 .265-.359V1.172a.4.4 0 0 0-.406-.39c-.031 0-.094 0-.14.015l-.641.219v-3.188l.922-.312a.426.426 0 0 0 .265-.391v-1.187a.414.414 0 0 0-.406-.407c-.031 0-.094 0-.14.032l-.641.234v-2.89a.367.367 0 0 0-.36-.376.391.391 0 0 0-.374.375v3.157l-1.657.609v-2.89a.38.38 0 0 0-.375-.376.367.367 0 0 0-.36.375v3.157l-.905.328A.379.379 0 0 0 0-2.375m3.563.484v3.188l-1.657.594v-3.188Zm0 0" fill="currentColor" id="b04"></path><path d="M.25 29.79h265.465M.25 24.805h265.465M.25 19.824h265.465M.25 14.844h265.465M.25 9.863h265.465" fill="currentColor" id="d08"></path><path d="M160.195 29.79V9.862M95.3 29.79V9.862M224.895 29.79V9.862m40.597 19.928V9.862" fill="currentColor" id="d09"></path><path d="M142.754 34.77h8.75m-52.371 0h8.75" fill="currentColor" id="d010"></path><path d="M201.113 21.188h.246V5.078h-.246Zm0 0" fill="currentColor" id="d011"></path><path d="M201.113 21.188h.246V5.081h-.246Zm0 0" fill="currentColor" id="d012"></path><path d="M186.156 23.68h.25V7.57h-.25Zm0 0" fill="currentColor" id="d013"></path><path d="M171.203 26.168h.246V10.063h-.246Zm0 0" fill="currentColor" id="d014"></path><path d="M171.2 26.172h.25v-16.11h-.25Zm0 0" fill="currentColor" id="d015"></path><path d="M210.309 36.23h.25V20.95h-.25Zm0 0" fill="currentColor" id="d016"></path><path d="M210.309 36.23h.25V20.953h-.25Zm0 0" fill="currentColor" id="d017"></path><path d="M106.309 33.64h.246V17.532h-.246Zm0 0" fill="currentColor" id="d018"></path><path d="M84.855 31.152h.25V15.043h-.25Zm0 0" fill="currentColor" id="d019"></path><path d="M84.855 31.152h.247V15.043h-.247Zm0 0" fill="currentColor" id="d020"></path><path d="M71.145 26.168h.25V10.063h-.25Zm0 0" fill="currentColor" id="d021"></path><path d="M71.145 26.172h.25v-16.11h-.25Zm0 0" fill="currentColor" id="d022"></path><path d="M136.219 28.66h.246V12.55h-.246Zm0 0" fill="currentColor" id="d023"></path><path d="M121.262 31.152h.25V15.043h-.25Zm0 0" fill="currentColor" id="d024"></path><path d="M149.926 33.64h.25V17.532h-.25Zm0 0" fill="currentColor" id="d025"></path><path d="M.25 29.785h329.723M.25 24.805h329.723M.25 19.824h329.723M.25 14.844h329.723M.25 9.863h329.723" fill="currentColor" id="d026"></path><path d="M200.219 34.77h7.996m-36.805 0h8.371m-44.321 0h7.993m-44.32 0h8.37" fill="currentColor" id="d027"></path><path d="M289.148 29.785V9.863M95.3 29.785V9.863m129.153 19.922V9.863m105.293 19.922V9.863" fill="currentColor" id="d028"></path><path d="M218.367 28.66h.25V14.11h-.25Zm0 0" fill="currentColor" id="d029"></path><path d="M235.46 26.168h.247V10.063h-.246Zm0 0" fill="currentColor" id="d030"></path><path d="M235.457 26.172h.25v-16.11h-.25Zm0 0" fill="currentColor" id="d031"></path><path d="M178.207 14.793v1.992l40.41-1.89v-1.993Zm0 0" fill="currentColor" id="d032"></path><path d="M178.207 36.133h.246v-20.16h-.246Zm0 0" fill="currentColor" id="d033"></path><path d="M178.203 36.133h.25v-20.16h-.25Zm0 0" fill="currentColor" id="d034"></path><path d="M189.934 31.152h.25V15.43h-.25Zm0 0" fill="currentColor" id="d035"></path><path d="M189.934 31.152h.246V15.43h-.246Zm0 0" fill="currentColor" id="d036"></path><path d="M206.64 33.64h.25V14.657h-.25Zm0 0" fill="currentColor" id="d037"></path><path d="M106.309 14.793v1.992l27.21-.945v-1.992Zm0 0" fill="currentColor" id="d038"></path><path d="M106.309 18.828v1.992l27.21-.945v-1.992Zm0 0" fill="currentColor" id="d039"></path><path d="M142.258 14.793v1.992l27.21-.945v-1.992Zm0 0" fill="currentColor" id="d040"></path><path d="M142.258 18.828v1.992l27.21-.945v-1.992Zm0 0" fill="currentColor" id="d041"></path><path d="M274.566 36.23h.25V20.95h-.25Zm0 0" fill="currentColor" id="d042"></path><path d="M274.566 36.23h.25V20.953h-.25Zm0 0" fill="currentColor" id="d043"></path><path d="M250.414 23.68h.25V7.57h-.25Zm0 0" fill="currentColor" id="d044"></path><path d="M265.367 21.188h.25V5.078h-.25Zm0 0" fill="currentColor" id="d045"></path><path d="M265.367 21.188h.25V5.081h-.25Zm0 0" fill="currentColor" id="d046"></path><path d="M106.309 33.64h.246V15.978h-.246Zm0 0" fill="currentColor" id="d047"></path><path d="M115.293 31.152h.25V15.668h-.25Zm0 0" fill="currentColor" id="d048"></path><path d="M115.293 31.152h.25v-15.48h-.25Zm0 0" fill="currentColor" id="d049"></path><path d="M142.258 33.64h.246V15.978h-.246Zm0 0" fill="currentColor" id="d050"></path><path d="M160.23 28.66h.25V15.363h-.25Zm0 0" fill="currentColor" id="d051"></path><path d="M151.242 31.152h.25V15.668h-.25Zm0 0" fill="currentColor" id="d052"></path><path d="M151.242 31.152h.25v-15.48h-.25Zm0 0" fill="currentColor" id="d053"></path><path d="M124.281 28.66h.25V15.363h-.25Zm0 0" fill="currentColor" id="d054"></path><path d="M169.219 31.152h.25V15.055h-.25Zm0 0" fill="currentColor" id="d055"></path><path d="M133.27 31.152h.25V15.055h-.25Zm0 0" fill="currentColor" id="d056"></path><path d="M133.27 31.152h.246V15.055h-.246Zm0 0" fill="currentColor" id="d057"></path></defs><use fill="none" href="#d08" stroke="currentColor" stroke-linecap="round" stroke-miterlimit="10" stroke-width=".498"></use><use fill="none" href="#d09" stroke="currentColor" stroke-miterlimit="10" stroke-width=".947"></use><use href="#a00" x="65.096" y="27.297"></use><use fill="none" href="#d019" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d020"></use><use href="#a00" x="78.806" y="32.278"></use><use fill="none" href="#d021" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d022"></use><use href="#d00" x="31.731" y="19.824"></use><use href="#e00" x="22.018" y="19.824"></use><use href="#f00" x="3.985" y="24.806"></use></svg></span><span class="pos"><span class="chord" style="left:7.0px;">C6</span><svg class="sheet-music-svg" height="38pt" viewBox="95.3 0.0 64.895 38.0" width="64.895pt" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use fill="none" href="#d08" stroke="currentColor" stroke-linecap="round" stroke-miterlimit="10" stroke-width=".498"></use><use fill="none" href="#d09" stroke="currentColor" stroke-miterlimit="10" stroke-width=".947"></use><use fill="none" href="#d010" stroke="currentColor" stroke-linecap="round" stroke-miterlimit="10" stroke-width=".996"></use><use fill="none" href="#d018" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d018"></use><use href="#a00" x="100.259" y="34.768"></use><use href="#a00" x="130.169" y="29.787"></use><use fill="none" href="#d023" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d023"></use><use fill="none" href="#d024" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d024"></use><use href="#a00" x="115.213" y="32.278"></use><use href="#a00" x="143.878" y="34.768"></use><use fill="none" href="#d025" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d025"></use></svg></span><span class="pos"><span class="chord" style="left:7.0px;">Dm</span><span class="chord" style="left:39.35000000000001px;">G7</span><svg class="sheet-music-svg" height="38pt" viewBox="160.195 0.0 64.70000000000002 38.0" width="64.70000000000002pt" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use fill="none" href="#d08" stroke="currentColor" stroke-linecap="round" stroke-miterlimit="10" stroke-width=".498"></use><use fill="none" href="#d09" stroke="currentColor" stroke-miterlimit="10" stroke-width=".947"></use><use href="#a00" x="210.11" y="19.824"></use><use fill="none" href="#d011" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d012"></use><use href="#a00" x="195.064" y="22.315"></use><use fill="none" href="#d013" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d013"></use><use href="#a00" x="180.108" y="24.806"></use><use fill="none" href="#d014" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d015"></use><use href="#a00" x="165.153" y="27.297"></use><use fill="none" href="#d016" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d017"></use><use href="#c00" x="204.899" y="19.824"></use></svg></span><span class="pos"><span class="chord" style="left:7.0px;">C</span><svg class="sheet-music-svg" height="38pt" viewBox="224.895 0.0 45.59700000000001 38.0" width="45.59700000000001pt" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use fill="none" href="#d08" stroke="currentColor" stroke-linecap="round" stroke-miterlimit="10" stroke-width=".498"></use><use fill="none" href="#d09" stroke="currentColor" stroke-miterlimit="10" stroke-width=".947"></use><use href="#b00" x="234.83" y="17.334"></use></svg></span></div>
</div>

e esse

```
           .          C6   ,  .   ,     Gm6  ,  A7     Dm , G7   C
% \time 4/4 s2 f,4 d | c16 d e d c d e d bes8 d cis e | f4 g a b | c1
```

resulta nisso:

<div class="cifra-document">
<div class="anchored-sheet-music line"><span class="pos"><span class="chord" style="left:12.0px;"> </span><svg class="sheet-music-svg" height="43pt" viewBox="0.0 0.0 95.3 43.0" width="95.3pt" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use fill="none" href="#d026" stroke="currentColor" stroke-linecap="round" stroke-miterlimit="10" stroke-width=".498"></use><use fill="none" href="#d028" stroke="currentColor" stroke-miterlimit="10" stroke-width=".947"></use><use href="#f00" x="3.985" y="24.806"></use><use href="#e00" x="22.018" y="19.824"></use><use href="#d00" x="31.731" y="19.824"></use><use href="#a00" x="65.096" y="27.297"></use><use fill="none" href="#d021" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d022"></use><use href="#a00" x="78.806" y="32.278"></use><use fill="none" href="#d019" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d020"></use></svg></span><span class="pos"><span class="chord" style="left:7.0px;">C6</span><span class="chord" style="left:39.28824999999999px;"> </span><span class="chord" style="left:71.57649999999998px;">Gm6</span><span class="chord" style="left:103.86474999999997px;">A7</span><svg class="sheet-music-svg" height="43pt" viewBox="95.3 0.0 129.15299999999996 43.0" width="129.15299999999996pt" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use fill="none" href="#d026" stroke="currentColor" stroke-linecap="round" stroke-miterlimit="10" stroke-width=".498"></use><use fill="none" href="#d027" stroke="currentColor" stroke-linecap="round" stroke-miterlimit="10" stroke-width=".996"></use><use fill="none" href="#d028" stroke="currentColor" stroke-miterlimit="10" stroke-width=".947"></use><use fill="none" href="#d029" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d029"></use><use fill="none" href="#d032" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d032"></use><use href="#a00" x="172.157" y="37.259"></use><use fill="none" href="#d033" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d034"></use><use href="#a00" x="183.884" y="32.278"></use><use fill="none" href="#d035" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d036"></use><use href="#a00" x="212.32" y="29.787"></use><use href="#a00" x="200.593" y="34.768"></use><use fill="none" href="#d037" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d037"></use><use href="#b04" x="193.37" y="34.768"></use><use fill="none" href="#d038" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d038"></use><use fill="none" href="#d039" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d039"></use><use fill="none" href="#d040" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d040"></use><use fill="none" href="#d041" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d041"></use><use href="#a00" x="100.259" y="34.768"></use><use fill="none" href="#d047" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d047"></use><use fill="none" href="#d048" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d049"></use><use href="#a00" x="109.246" y="32.278"></use><use href="#a00" x="136.208" y="34.768"></use><use fill="none" href="#d050" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d050"></use><use fill="none" href="#d051" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d051"></use><use href="#a00" x="154.182" y="29.787"></use><use href="#a00" x="145.195" y="32.278"></use><use fill="none" href="#d052" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d053"></use><use href="#a00" x="118.233" y="29.787"></use><use fill="none" href="#d054" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d054"></use><use fill="none" href="#d055" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d055"></use><use href="#a00" x="163.17" y="32.278"></use><use href="#a00" x="127.221" y="32.278"></use><use fill="none" href="#d056" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d057"></use></svg></span><span class="pos"><span class="chord" style="left:7.0px;">Dm</span><span class="chord" style="left:39.347500000000025px;">G7</span><svg class="sheet-music-svg" height="43pt" viewBox="224.45299999999997 0.0 64.69500000000005 43.0" width="64.69500000000005pt" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use fill="none" href="#d026" stroke="currentColor" stroke-linecap="round" stroke-miterlimit="10" stroke-width=".498"></use><use fill="none" href="#d028" stroke="currentColor" stroke-miterlimit="10" stroke-width=".947"></use><use href="#a00" x="229.41" y="27.297"></use><use fill="none" href="#d030" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d031"></use><use fill="none" href="#d042" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d043"></use><use fill="none" href="#d044" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d044"></use><use href="#a00" x="259.321" y="22.315"></use><use fill="none" href="#d045" stroke="currentColor" stroke-linejoin="round" stroke-miterlimit="10" stroke-width=".399"></use><use href="#d046"></use><use href="#a00" x="274.367" y="19.824"></use><use href="#a00" x="244.366" y="24.806"></use><use href="#c00" x="269.156" y="19.824"></use></svg></span><span class="pos"><span class="chord" style="left:7.0px;">C</span><svg class="sheet-music-svg" height="43pt" viewBox="289.148 0.0 45.597999999999956 43.0" width="45.597999999999956pt" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use fill="none" href="#d026" stroke="currentColor" stroke-linecap="round" stroke-miterlimit="10" stroke-width=".498"></use><use fill="none" href="#d028" stroke="currentColor" stroke-miterlimit="10" stroke-width=".947"></use><use href="#b00" x="299.088" y="17.334"></use></svg></span></div>
</div>


Note como combinamos pontos e vírgulas para denotar o ritmo desejado:
`2 4 4`

### Comando adicionais

Além de qualquer comando do LilyPond, podemos usar os seguintes:

 - `\staffsize <n>`: define o tamanho da pauta `(#set-global-staff-size)`.
   O padrão é 20.
 - `\hideall`: não mostra clave, armadura de clave nem fórmula de
   compasso.
 - `\noanchor`: não ancora os acordes acima da linha. Útil para definir
   espaçamento dos acordes manualmente
 - `\nobreak`: não insere quebras de linha quando se tem linhas de
   acordes alternadas com linhas de partitura
 - `\hshortest <fração>`: abreviação de
   `\override Score.SpacingSpanner.base-shortest-duration = #(ly:make-moment <fração>)`.
   Altera a unidade básica de espaçamento horizontal para ser a nota que
   corresponde à fração dada. Use assim: `\hshortest 1/16`
 - `\hinc <n>`: abreviação de
   `\newSpacingSection \override Score.SpacingSpanner.spacing-increment = #<n>`.
   Adiciona um pouco de espaço horizontal depois de cada nota
 - `\hincrev`: abreviação de
   `\newSpacingSection \revert Score.SpacingSpanner.spacing-increment`.
   Reverte o `\hinc` anterior

### Valores padrão

 - Fórmula de compasso: `2/4`
 - Tom: Oriundo do campo "Tom" do cabeçalho da cifra. Na ausência deste,
   usa-se `c \major` (dó maior).
 - Clave: `treble`
 - `relative`: `c''`

### Mais informações

A interpretação de partituras é baseado no software
[`svgly`](https://gitlab.com/joaoseckler/svgly), que toma como entrada
expressões lilypond e devolve SVGs otimizados para a web.
