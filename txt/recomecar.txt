Recomeçar - Paulinho da Viola | Elton Medeiros
Intérprete: Paulinho da Viola
Álbum: Zumbido
Ano: 1979
Tom: A
Forma: ~A~B ~A~B ~A~, ~intro~ à vontade
Tessitura: A3 - E5
BPM: 113
Cifraclub: https://www.cifraclub.com.br/paulinho-da-viola/recomecar/
Catálogo: https://www.discogs.com/release/3046949-Paulinho-Da-Viola-Zumbido

% \nobreak \repeat volta 2 {
    D#m7(b5)                  E7
%   \hinc 3
%   cis4_"lalalaiá..." b8 a | \hincrev gis8. fis16~ \sinc fis e gis~ |
    C#m
%   gis4 fis8 e |
    F#7                               B7
%   d8. cis16~ \sinc cis fis cis~ | cis2 |
%   \alternative {
        E7          Em   A7
%     { b4.. e16~ | e2 | r2 | }
        E7          A    E7
%     { b4.. a16~ | a2 | r2 | }
%   }
% }

[A]

A7M   Bm7
Recomeçar
E7                      C#m7
   Do que restou de uma paixão
          F#m7         B7
Voltar de novo à mesma dor sem razão
Bm7           E7            A7M
   Guardar no peito a mágoa sem reclamar
C#m7(b5)   F#7           Bm
     Acreditar no sol da nova manhã
        E7            A7M
Dizer a deus e renunciar
C#m7(b5)   F#7       Bm
  Vestir a capa de cobrir solidão
       E7     A7M
Para poder chorar

[B]

          F#7         B7
Somente o tempo faz a gente lembrar
Bm7        E7            A7M
   Do sofrimento que não quis perdoar
   F#m                F#m6
E     todo o mal reprimido
B7                 Bm7    E7
Pode afinal nos deixar

A7M       F#7          B7
   A vida tem seu renascer de uma dor
Bm7       E7      C#m7(b5)      F#7
   Toda ferida um dia tem que fechar
D#m7(b5)   E7       C#m7
E   quem secou esse pranto
F#m      B7    E7   A7M
Pode novamente a____mar.

       E7
(Recomeçar!)
