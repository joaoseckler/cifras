Não quero saber mais dela - Sombrinha | Almir Guineto
Intérprete: Arlindo Cruz | Sombrinha
Álbum: Pra ser feliz
Ano: 1998
Tom: G
Forma: AB1 AB2 A
Tessitura: B2 - A4
BPM: 120
Cifraclub: https://www.cifraclub.com.br/fundo-de-quintal/nao-quero-mais-saber-dela/
Catálogo: https://immub.org/album/pra-ser-feliz

% \nobreak \partial 16*3
  .         C
% g8 e16~ | \sinc e fis g b16 a b a~ |
  Cm
% \sinc a g fis~ \sinc fis g d'~ |
  Bm
% d16 b d b~ b a g fis~ |
  E7
% \sinc fis e dis~ \sinc dis e g~ |
  A7
% \sinc g cis, d e16 fis g fis~ |
  D7
% fis8 d16 e fis g a b |
  G                   D7
% g8. fis16 a g fis e | d8 r4.

[A]

G              D7       G
Eu não quero saber mais dela
     D7          G     D7
Foi embora da Portela
              Bm5-/7   E7
Dizendo que o samba
   Am                 G7
Lá em Mangueira que é bom

C               F7
Ah! meu Deus do céu
                Bm
Minha doce companheira
       F7         E7(4)
(se mandou de Madureira)
      E7         Am
Se mandou de madureira
D7                    Dm
   Seja o que Deus quiser
                G7
Foi morar no chalé

C               F7
Ah! meu Deus do céu
                Bm
Minha doce companheira
       F7         E7(4)
(se mandou de Madureira)
      E7         Am
Se mandou de Madureira
D7                    G
   Seja o que Deus quiser

[B1]

      E7          Am
Foi embora porque quis
           D7       G
Com uma atitude leviana
                  Dm
Mudou da água pro vinho
            G7           C
Pensando em conquistar a fama
          Cm          F7           Bm
Mas fique certa que jamais terá perdão
E7                    A7
Quando a cabeça não pensa
          D7           G
Quem paga caro é o coração
                D7
(eu não quero saber !)


[B2]

E7          Am
Vou cair no samba
     D7           G
Esquecer o que passou
                             Dm
Qualquer dia ela passa lá em casa
              G7                  C
Querendo explicar porque ma abandonou
           Cm             F7             Bm
Mas não adianta se ela voltar, não quero mais
E7             A7
É como diz o ditado
           D7              G
Palavra de rei não volta atrás
                 D7
( eu não quero saber! )
