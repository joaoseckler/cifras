Doce refúgio - Luiz Carlos da Vila
Intérprete: Fundo de quintal
Álbum: Fundo de quintal ao vivo - Fundo de Quintal
Ano: 2000
Forma: ABA
Obs: Pra emendar com caciqueando eles não cantam o segundo verso, por isso ABA.
Tom: G
Tessitura: D3 - G4
BPM: 95
Cifraclub: https://www.cifraclub.com.br/fundo-de-quintal/doce-refugio/
Catálogo: https://immub.org/album/simplicidade-fundo-de-quintal-ao-vivo


G   G5+   G6   G5+  ...

[A]

G    G5+   G6
Sim
G5+            G     G5+   G6
é o Cacique de Ramos
G5+                  G      G5+
Planta onde todos os ramos
Bm7(5-)        E7           Am    G
cantam os passarinhos nas manhãs
C        F7(9)                   Bm7(5-)   E7
Lá (oi é lá)   o samba é alta bandeira
               A7
E até as tamarineiras
Am7       D7        Dm7   G7
são da poesia guardiãs
C        F7(9)                 Bm7(5-)   E7
Lá (oi é lá) o samba é alta bandeira
               A7
E até as tamarineiras
Am7       D7        G
são da poesia guardiãs

[B]

            E7     Am
Seus compositores aqueles
              D7              G
Que deixam na gente aquela emoção
                    Dm7
Seus ritmistas vão fundo
            G7                 C
Tocando bem fundo qualquer coração
               Cm
É uma festa brilhante
             F7(9)                Bm7(5-)
Um lindo brilhante mais fácil de achar
           E7            A7
É perto de tudo ali no subúrbio
           D7                    G
Um doce refúgio pra quem quer cantar
       D7
É o cacique

[B']

E7              Am                D7
É o Cacique pra uns a cachaça pra outros
        G
A religião
                           Dm7
Se estou longe o tempo não passa
               G7            C
E a saudade abraça o meu coração
                       Cm
Quando ele vai para as ruas
          F7(9)           Bm7
A vida flutua num sonho real
            E7                 A7
É o povo sorrindo Cacique esculpindo
               D7              G
Com mãos de alegria o seu carnaval
        D7
É o cacique
