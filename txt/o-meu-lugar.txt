O meu lugar - Arlindo Cruz | Mauro Diniz
Intérprete: Arlindo Cruz
Álbum: Sambista perfeito
Ano: 2007
Tom: Am
Forma: ABR ABR C R DEE, meio R
Tessitura: E3 - G4
BPM: 74
Cifraclub: https://www.cifraclub.com.br/arlindo-cruz/o-meu-lugar/
Catálogo: https://immub.org/album/sambista-perfeito


Am7    Am6      Am7
Dm7    G7(4/9)  Am7

[A]

Am7(11)                    D7(9)
O meu lugar é caminho de Ogum e Iansã
               Dm7(9)     G7(13)
Lá tem samba até de manhã
              C6(9)     Bm7  E7(9-)
Uma ginga em cada andar

Am7         Am/G
O meu lugar
              D7(9)
É cercado de luta e suor
               Dm7(9)      G7(4/9)
Esperança num mundo melhor
            Gm7      C7(9)
E cerveja pra comemorar


[B]

Fm7
O meu lugar tem seus mitos
   C           Am7
E seres de luz
               D7(9)
É bem perto de Osvaldo Cruz, Cascadura
     Bm7(5-)     E7(b13)
Vaz Lobo e Irajá
Am7(11)                   D7(9)
O meu lugar é sorriso, é paz e prazer
              Dm7(9)    G7(4/9)
O seu nome é doce dizer

[R]

       C6(9)  Ebº  Dm7  G7(4/9)
Madureira,    lá laiá
       C6(9)  Ebº   Dm7  G7(4/9)  E7(9-/13-)
Madureira,    lá  laiá


[C]

Am7                          Em7
Ah, que lugar! A saudade me faz relembrar
                  Bm7(5-)    E7(9+)
Os amores que eu tive por lá
              Am7   B7(13)  E7(9+)
É difícil esquecer
 Am7                         Em7
Doce lugar, que é eterno no meu coração
                  B7(4)      B7
E aos poetas traz inspiração
                   E7M(9)  E7(4/9)
Pra cantar e escrever


I
A7M
Ai, meu lugar!
                    C#7
Quem não viu Tia Eulália dançar?
                D6(9)        E7(4)
Vó Maria, o terreiro benzer?
                             i
             A7M             Em7  A7
E ainda tem jongo à luz do luar
Dm7
Ai, que lugar! Tem mil coisas
     Am7
Pra gente dizer
Am6                Dm7         G7(4/9)
    O difícil é saber terminar

[D]

            IIIb
            Em7        Ebº      Dm7
Em cada esquina, um pagode num bar
        G7(4/9)  C6(9)
Em Madurei______ra
              Em7         Ebº    Dm7
Império e Portela também são de lá
        G7(4/9)   C7(4/9)
Em Madurei_______ra

           F#7(9/11+)        F7M
E no Mercadão você pode comprar
            Fm7M              Em7
Por uma pechincha você vai levar
              Eb°                    Dm7
Um dengo, um sonho pra quem quer sonhar
        G7(4/9)  C7(4/9)
Em Madurei______ra

               F#7(9/11+)       F7M
E quem se habilita até pode chegar
             Fm7M               Em7
Tem jogo de lona, caipira e bilhar
          Eb°               Dm7
Buraco, sueca pro tempo passar
     G7(4/9)  C7(4/9)
Madurei______ra

         F#7(9/11+)        F7M
E uma fezinha até posso fazer
             E7(9+)             Am7
No grupo, dezena, centena e milhar
            Eb7(9/11+)         Dm7
E nos sete lados eu vou te cercar
     G7(4/9)  C6(9)
Madurei______ra

[E]

        C7(13)          F7M
La la laia la laia la laia
        Fm7M            C6(9)
La la laia la laia la laia
        Ebº             Dm7
La la laia la laia la laia

       G7(4/9)  C6(9)
Em Madurei______ra
