# Cifras

Collection of lyrics with chords ("cifras" in Portuguese). Mostly of
sambas.

These cifras are used within a webpage (supporting sheet music,
transposition, autoscrolling, line breaking, songbook generation, and
more) available [here](https://jseckler.xyz/cifras). You can also check
out the code for the [frontend](https://gitlab.com/joaoseckler/laialaia)
and [backend](https://gitlab.com/joaoseckler/website/-/tree/master/laialaia).

## Creating a cifra

Simply write it and put it in the txt folder. Some header information is
expected: see the [template file](template.txt) for the basic structure:

```txt
<title> - <author>
Intérprete: <interpreters>
Álbum: <album>
Ano: <year>
Tom: <key>
Forma: <form>
Obs: <observation>
Tessitura: <tessitura (melody range)>
BPM: <beats per minute>
Cifraclub: <link to same song in cifraclub.com.br>
Catálogo: <link to some cataloging page for this song>
```

(This software is partially in Brazilian Portuguese: if you are
interested in an completely English version, please contact me or open
an issue.)

### Header options

Except the ones listed below, all header options will appear in the
resulting webpage (though some of them may be combined in some way):
 - Alt: indicates that this is the alternative version of the cifra (see
   section above)
 - Main: indicates that this is the main version of a cifra

### Writing different versions of the same cifra

 - Write another cifra text file inside the `txt` folder, using the same
   title;
 - Use `Alt: yes` in the header to indicate that it is not the main
   version. Alternatively, use `Main: yes` to indicate it is.

## Writing sheet music inside cifras

To write lines of sheet music, start a line with the `%` character, and
start writing [LilyPond](https://lilypond.org) music expressions. You
can use multiple adjacent lines starting with % to write a single line
of sheet music. If there is a line with chords immediately above the
sheet music line, these chords will be anchored to the measures of the
sheet music. Use a dot (`.`) to signal that a measure shouldn't have any
chords over it. A comma (`,`) indicates that the following chord should
be in the same measure as the previous one. This operation can be
concatenated to have several chords in the same measure.

#### Example

The following snippet:

```
           .          C6          Dm , G7   C
% \time 4/4 s2 f,4 d | c4 d e c | f4 g a b | c1
```

Yields:

![Output of example above](img/example1.png "Example 1")

And this one:

```
           .          C6   ,  .   ,     Gm6  ,  A7     Dm , G7   C
% \time 4/4 s2 f,4 d | c16 d e d c d e d bes8 d cis e | f4 g a b | c1
```

Yields this:

![Output of example above](img/example2.png "Example 2")

Notice that we combined commas and dots to form the intended rhythm:
`2 4 4`.

If you write alternated sheet music and chord lines without any space
between them, they will be evaluated together and `<br />` tags will be
inserted where the chord lines break (see `\nobreak` option below to
avoid this behavior).

#### Using parentheses

You can use parentheses around chords above sheet music. The left and
right parentheses will be joined with the following and preceding
chords, respectively, with a non-breaking space. Don't use a comma next
to them.

#### Additional commands

Besides any lilypond command, our interface offers the following
commands:

 - `\staffsize <n>`: set staff size using `(#set-global-staff-size)`.
   Default is 20.
 - `\hideall`: don't show clef, key signature nor time signature
 - `\noanchor`: don't anchor the chords above to this line. Useful for
   manually choosing the placements of chords.
 - `\nobreak`: don't break lines when writing alternated sheet music
   lines and chord lines
 - `\hshortest <fraction>`: convenience shorcut for
 `\override Score.SpacingSpanner.base-shortest-duration = #(ly:make-moment <fraction>)`.
 It changes the base unit of horizontal spacing to be the note
 corresponding to the given fraction.
 - `\hinc <n>`: convenience shortcut for
 `\newSpacingSection \override Score.SpacingSpanner.spacing-increment = #<n>`.
 It increments a bit of space after each note.
 - `\hincrev`: convenience shortcut for
 `\newSpacingSection \revert Score.SpacingSpanner.spacing-increment`. It
 reverses the previous `\hinc`

#### Default values

 - Time signature: `2/4`
 - Key: Taken from the "Tom" header attribute. Falls back to `c \major`
 - Clef: `treble`
 - relative: `c''`

#### More on sheet music parsing

Parsing relies on [`svgly`](https://gitlab.com/joaoseckler/svgly), which
takes LilyPond expressions and outputs svgs optimized for the web.

## Markdown pages

You can read about the reasoning, standards, and more detailed
information in the "about" pages inside the [`md`](md) folder (or their
[web versions](https://jseckler.xyz/cifras/sobre)).
