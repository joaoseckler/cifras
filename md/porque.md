description: Princípios norteadores deste site

# Porque esse site e não o cifraclub?

O [cifraclub](https://cifraclub.com.br), a despeito de quaisquer
defeitos que dele se possa dizer, é com certeza um patrimônio para todos
que fazem ou se beneficiam de música. Esse site não se propõe,
naturalmente, substituí-lo, mas sim especializar sua função. Encorajamos
que cifras sejam corrigidas e enviadas lá também.

Segue uma comparação deste site com o CifraClub, visando enunciar alguns
de nossos princípios norteadores deste site.

## Sucintez

Muitas cifras no cifraclub lidam com a repetição simplesmente
escrevendo-a por extenso. Isso é muito útil se você está usando uma tela
pequena, onde é mais difícil navegar pelas partes da cifra. Basta ativar
a autorolagem e seguir em frente.

A abordagem aqui é a de não repetir partes na cifra, mas sim
identificá-las por nomes e descrever a forma da música usando-as. Isso
beneficia quem usa telas grandes e quem imprime a cifra em papel
(principalmente considerando a opção de usar mais de uma coluna). Se sua
experiência com esse site no celular estiver ruim por conta disso, pode
valer a pena recorrer ao cifraclub.

Também há cifras sucintas e indicação de seções no cifraclub, mas lá
isso não é uma regra.

## Cifras moderadas

As cifras aqui são moderadas para (espera-se) oferecer cifras com menos
erros. Essa moderação é feita, em público, no GitLab, por meio de
[Issues](https://gitlab.com/joaoseckler/cifras/-/issues) e de [Merge
Requests](https://gitlab.com/joaoseckler/cifras/-/merge_requests), e
atualmente conta com um mantenedor. A expectativa é que isso evolua para
um sistema de moderação [como o da rede Stack
Exchange](https://stackoverflow.blog/2009/05/18/a-theory-of-moderation/).

Também há cifras "moderadas" no cifraclub (muitas, inclusive, com
video-aula): a diferença é que não são todas.

## Partitura ao invés de tablatura

A notação musical escolhida para extender a cifra, onde esta se mostra
limitada, é a notação musical convencional. No cifraclub isso não é
possível. A notação escolhida lá é a tablatura, cuja desvantagem é
é só servir para instrumentos de corda com uma determinada afinação.

## Formatação mais estrita

A formatação de cifras deve seguir um
[padrão mais estrito](escrever#combinados) que a do cifraclub, para
padronizar e facilitar a leitura. Essa formatação aqui é obrigatória.

## Versões

O cifraclub organiza suas músicas por artista e título. As versões se
referem, geralmente, à instrumentação e à dificuldade. Aqui
considera-se que uma cifra pode ser lida por qualquer músico,
independente de seu instrumento, e buscamos sempre ser o mais fiéis o
possível a uma _versão_ em questão. Portanto, organizamos as músicas
somente por título, cada um correspondendo a uma ou mais versões.

De preferência, uma _versão_ está estritamente associada a um fonograma,
mas há casos em que não há um disponível ou não é interessante se ater a
ele. Uma versão poderia, por exemplo, se referir ao modo como tal grupo
interpreta tal música: é importante, todavia, que essa referência seja
constante a ponto de poder se avaliar se a cifra corresponde a ela ou
não.

## Mais metadados

O cifraclub armazena e disponibiliza vários metadados da música: título,
artista, compositor, tom e versão. Aqui nos propomos armazenar mais
informações: tempo, tessitura da melodia e forma. Também é possível
armazenar qualquer quantidade de metadados arbitrários.
