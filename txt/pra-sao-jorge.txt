Pra São Jorge - Pece Ribeiro
Intérprete: Zeca Pagodinho
Álbum: À Vera
Ano: 2005
Tom: Bb
Forma: intro, P AA BB CC, PP AA BB CC, PP, intro à vontade, coda
Tessitura: G3 - F4
BPM: 120
Cifraclub: https://www.cifraclub.com.br/zeca-pagodinho/pra-sao-jorge/
Catálogo: https://immub.org/album/a-vera



[intro]

Bb
Lá la iá lá iá...........  Vamos saudar São Jorge Cavaleiro!

[P]

Eb   F   Bb   .
Eb   F   Bb   F

[A]

         Eb          F7     Bb
Vou ascender velas para São Jorge
         Eb     F7   Bb
A ele eu quero agradecer
          Eb         F7      Bb    Bb7
E vou plantar comigo-ninguém-pode
           Eb              F7     Bb
Para que o mal não possa então vencer

[B]

     Eb                Bb       F7   Bb
Olho grande em mim não pega não pega não
    Eb               Gm7   F7  Bb
Não pega em quem tem fé no coração

[C]

 Eb            Bb       Eb        Bb
Ogum com sua espada sua capa encarnada
   Cm7       F7   Bb
Me dá sempre proteção
 Cm7                  Bb/D   Eb              Bb/F
Quem vai pela boa estrada no fim dessa caminhada
     F7               Bb
Encontra em Deus perdão

[coda]

F#7M
   ( Ogum meu pai segura nós! )
